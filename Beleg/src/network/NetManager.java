package network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;


// TODO: Socket connection state needs to be checked
public class NetManager {

    public static final int PORT = 12345;
    public int verbose = 0;

    private static class InstanceHolder {
        private static NetManager INSTANCE = new NetManager();
    }

    public boolean run = true;

    private Map<ENetMsgType, INetMsgHandler> handlers = new HashMap<>();

    private ServerSocket server;
    private final List<Socket> sockets = new ArrayList<>();

    private BlockingDeque<NetMsg> msgs = new LinkedBlockingDeque<>();

    private Thread handlerThread;
    private Thread receiveThread;
    private Thread acceptThread;


    private NetManager() {}


    public static NetManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public boolean init(ENetManagerType type, String host) {
        boolean result;

        NetManager netManager = getInstance();

        if (type == ENetManagerType.SERVER) {
            result = netManager.initServer();
        }
        else {
            result = netManager.connectToServer(host);
        }

        if (!result) {
            return false;
        }

        netManager.receiveThread = new Thread(netManager::receive);
        netManager.receiveThread.start();

        netManager.handlerThread = new Thread(netManager::handle);
        netManager.handlerThread.start();

        return true;
    }

    private boolean initServer() {
        boolean result;

        try {
            server = new ServerSocket(PORT);
            result = true;

            acceptThread = new Thread(this::accept);
            acceptThread.start();

        } catch (IOException io) {
            io.printStackTrace();
            result = false;
        }
        return result;
    }

    private boolean connectToServer(String ip) {
        boolean result = false;

        int tries = 5;

        for (int i = 0; i < tries; i++) {
            try {
                Socket server = new Socket(ip, PORT);

                synchronized (sockets) {
                    sockets.add(server);
                }

                result = true;
                break;
            } catch (IOException io) {
                io.printStackTrace();
                result = false;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }

        return result;
    }

    private void accept() {
        while (run) {
            try {
                Socket client = server.accept();

                synchronized (sockets) {
                    sockets.add(client);
                }

                if (verbose > 0) {
                    System.out.println("Client connected!");
                }

            } catch (IOException io) {
                io.printStackTrace();
            }
        }
    }

    // TODO: check success
    public void send(Socket socket, NetMsg msg) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeUnshared(msg);
            oos.flush();

            if (verbose > 0) {
                System.out.println("Msg send!");
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public void send(NetMsg msg) {
        List<Socket> socketsCopy = new ArrayList<>(sockets);

        for (Socket socket : socketsCopy) {
            try {
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeUnshared(msg);
                oos.flush();

                if (verbose > 0) {
                    System.out.println("Msg send!");
                }
            } catch (IOException io) {
                io.printStackTrace();
            }
        }
    }

    private void receive() {
        // TODO: CPU Load
        while (run) {

            List<Socket> socketsCopy = new ArrayList<>(sockets);

            for (Socket socket : socketsCopy) {
                try {
                    if (socket.getInputStream().available() > 0) {

                        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                        NetMsg msg = (NetMsg) ois.readObject();

                        // TODO: hacky should not be done this way
                        if (msg.type() == ENetMsgType.LOGIN) {
                            LoginMsg loginMsg = (LoginMsg) msg;
                            loginMsg.socket = socket;
                        }

                        msgs.offer(msg);

                        if (verbose > 0) {
                            System.out.println("New msg added!");
                        }
                    }
                } catch (IOException io) {
                    io.printStackTrace();
                } catch (ClassNotFoundException cnfe) {
                    cnfe.printStackTrace();
                }
            }
        }
    }

    public void register(INetMsgHandler<? extends NetMsg> handler) {
        handlers.put(handler.type(), handler);
    }

    private void handle() {
        while (run) {
            try {
                NetMsg msg = msgs.take();

                @SuppressWarnings("unchecked")
                INetMsgHandler<NetMsg> handler = handlers.get(msg.type());

                if (handler != null) {
                    // TODO: java generic type stuff
                    handler.handle(msg);
                } else {
                    // TODO: if there is no handler, maybe just drop the msg
                    msgs.offer(msg);
                }

            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}
