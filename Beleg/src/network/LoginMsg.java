package network;

import java.net.Socket;

public class LoginMsg extends NetMsg {

    public String name;
    public Socket socket;

    @Override
    public ENetMsgType type() {
        return ENetMsgType.LOGIN;
    }
}
