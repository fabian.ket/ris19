package network;

public enum ENetMsgType {
    CHAT, LOGIN, LOGOUT, PLAYER, PLAYER_NUMBER
}
