package network;

public class PlayerNumberMsg extends NetMsg {
    public int playerNumber;

    @Override
    public ENetMsgType type() {
        return ENetMsgType.PLAYER_NUMBER;
    }
}
