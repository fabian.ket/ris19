package network;

import org.joml.Matrix4f;

public class PlayerMsg extends NetMsg {

    public Matrix4f modelMat;
    public int player;

    @Override
    public ENetMsgType type() {
        return ENetMsgType.PLAYER;
    }
}
