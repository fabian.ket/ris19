import game.Game;

public class Main {

    public static void main(String[] args) {
        Game game = Game.getInstance();

        if (!game.init()) {
            System.out.println("Could not init game!");
            return;
        }


        if (!game.loop()) {
            System.out.println("Error in game loop!");
            return;
        }


        if (!game.exit()) {
            System.out.println("Error during exiting game!");
            return;
        }

        System.out.println("Working Directory = " + System.getProperty("user.dir"));
    }
}
