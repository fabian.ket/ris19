package client;

import network.*;

public class Main {
    public static void main(String[] args) {
        NetManager netManager = NetManager.getInstance();

        if (!netManager.init(ENetManagerType.CLIENT, null)) {
            System.out.println("Could not connect!");
            return;
        }

        netManager.verbose = 1;

        ChatMsg msg = new ChatMsg();

        int i = 0;
        while (true) {
            msg.data = "Hallo world! " + (++i);
            netManager.send(msg);
        }
    }
}
