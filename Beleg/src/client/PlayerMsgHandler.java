package client;

import game.Game;
import network.ENetMsgType;
import network.INetMsgHandler;
import network.PlayerMsg;

public class PlayerMsgHandler implements INetMsgHandler<PlayerMsg> {
    @Override
    public void handle(PlayerMsg msg) {
        System.out.println("Got PlayerMsg");

        synchronized (Game.getInstance().playerMsgs) {
            Game.getInstance().playerMsgs.add(msg);
        }
    }

    @Override
    public ENetMsgType type() {
        return ENetMsgType.PLAYER;
    }
}
