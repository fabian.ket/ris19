package client;

import game.Game;
import network.ENetMsgType;
import network.INetMsgHandler;
import network.PlayerNumberMsg;

public class PlayerNumberMsgHandler implements INetMsgHandler<PlayerNumberMsg> {
    @Override
    public void handle(PlayerNumberMsg msg) {
        Game.getInstance().playerNumber = msg.playerNumber;

        System.out.println("Number: " + msg.playerNumber);
    }

    @Override
    public ENetMsgType type() {
        return ENetMsgType.PLAYER_NUMBER;
    }
}
