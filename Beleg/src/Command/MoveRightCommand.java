package command;

import game.Game;
import game.GameObject;
import org.joml.Vector3f;

public class MoveRightCommand extends Command {

    @Override
    public void execute(GameObject obj) {
        obj.getModelMat().translate(new Vector3f(1 * Game.MOVE_SPEED, 0, 0));
    }
}
