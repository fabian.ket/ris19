package command;
import game.GameObject;

public abstract class Command {

    public abstract void execute(GameObject obj);
}
