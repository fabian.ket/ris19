package server;

import network.*;

public class LoginMsgHandler implements INetMsgHandler<LoginMsg> {

    @Override
    public void handle(LoginMsg msg) {

        PlayerNumberMsg playerNumberMsg = new PlayerNumberMsg();

        synchronized (Main.players) {
            playerNumberMsg.playerNumber = Main.players++;
        }

        // TODO: sometimes not send?
        NetManager.getInstance().send(msg.socket, playerNumberMsg);
    }

    @Override
    public ENetMsgType type() {
        return ENetMsgType.LOGIN;
    }
}
