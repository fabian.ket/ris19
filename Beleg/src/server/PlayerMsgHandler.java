package server;

import network.ENetMsgType;
import network.INetMsgHandler;
import network.NetManager;
import network.PlayerMsg;

public class PlayerMsgHandler implements INetMsgHandler<PlayerMsg> {
    @Override
    public void handle(PlayerMsg msg) {
        NetManager.getInstance().send(msg);
    }

    @Override
    public ENetMsgType type() {
        return ENetMsgType.PLAYER;
    }
}
