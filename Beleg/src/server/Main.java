package server;

import network.ENetManagerType;
import network.NetManager;

public class Main {

    // TODO: needs to be done in game class
    public static Integer players = 0;

    public static void main(String[] args) {

        NetManager netManager = NetManager.getInstance();

        if (!netManager.init(ENetManagerType.SERVER, null)) {
            System.out.println("Could not connect!");
            return;
        }

        netManager.verbose = 1;

        ChatMsgHandler chatHandler = new ChatMsgHandler();
        LoginMsgHandler loginHandler = new LoginMsgHandler();
        PlayerMsgHandler playerHandler = new PlayerMsgHandler();

        netManager.register(chatHandler);
        netManager.register(loginHandler);
        netManager.register(playerHandler);

    }
}
