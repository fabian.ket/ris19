package game;

import command.*;
import graphics.Window;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;

public class InputHandler {

    private Command btn_W;
    private Command btn_A;
    private Command btn_S;
    private Command btn_D;

    public void init() {

        btn_W = new MoveUpCommand();
        btn_A = new MoveLeftCommand();
        btn_S = new MoveDownCommand();
        btn_D = new MoveRightCommand();
    }

    public List<Command> handle() {

        List<Command> cmds = new ArrayList<>();

        long window = Window.getInstance().getId();

        if (window < 0) {
            return cmds;
        }

        if ( glfwGetKey(window, GLFW_KEY_W) == GLFW_TRUE ) {
            cmds.add(btn_W);
        }
        else if ( glfwGetKey(window, GLFW_KEY_S) == GLFW_TRUE ) {
            cmds.add(btn_S);
        }

        if ( glfwGetKey(window, GLFW_KEY_D) == GLFW_TRUE ) {
            cmds.add(btn_D);
        }
        else if ( glfwGetKey(window, GLFW_KEY_A) == GLFW_TRUE ) {
            cmds.add(btn_A);
        }

        return cmds;
    }
}
