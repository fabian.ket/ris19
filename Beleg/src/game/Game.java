package game;

import client.PlayerMsgHandler;
import client.PlayerNumberMsgHandler;
import command.Command;
import graphics.*;
import network.ENetManagerType;
import network.LoginMsg;
import network.NetManager;
import network.PlayerMsg;
import org.joml.Matrix4f;
import utils.Time;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

public class Game {

    private static class InstanceHolder {
        private static Game INSTANCE = new Game();
    }

    private Game() {}

    public static Game getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public int playerNumber = -1;

    public static final int WINDOW_WIDTH  = 800;
    public static final int WINDOW_HEIGHT = 450;
    public static final float FPS = 1.f / 60.f;
    public static final float MOVE_SPEED = 50 * FPS;

    // graphics
    private Window window = Window.getInstance();
    private Camera cam = new Camera();
    private Renderer renderer = new Renderer();

    // input
    private InputHandler input = new InputHandler();

    // game attributes
    private PlayerObject player = new PlayerObject();
    private Map<Integer, PlayerObject> players = new HashMap<>();
    private List<GameObject> staticObjects = new ArrayList<>();

    public List<PlayerMsg> playerMsgs = new ArrayList<>();

    public boolean init() {

        // INIT Window
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.init();

        // INIT Camera
        cam.init(WINDOW_WIDTH, WINDOW_HEIGHT);

        // INIT InputHandler
        input.init();

        // INIT Renderer
        renderer.init("shader");

        Model quad = new Model();
        quad.init(Model.VERTICES, Model.INDICES, Model.TEXTURE_COORDINATES);

        Texture texture = new Texture();
        texture.init(Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.TEXTURE);

        player.init(quad, texture);


        int wallSize = 20;

        // Default quad
        float[] wallTopVertices = new float[] {
                -WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2, 0f, // TOP LEFT
                 WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2, 0f, // TOP RIGHT
                 WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 - wallSize, 0f, // BOTTOM RIGHT
                -WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 - wallSize, 0f, // BOTTOM LEFT
        };

        float[] wallBottomVertices = new float[] {
                -WINDOW_WIDTH / 2, -WINDOW_HEIGHT / 2 + wallSize, 0f, // TOP LEFT
                 WINDOW_WIDTH / 2, -WINDOW_HEIGHT / 2 + wallSize, 0f, // TOP RIGHT
                 WINDOW_WIDTH / 2, -WINDOW_HEIGHT / 2, 0f, // BOTTOM RIGHT
                -WINDOW_WIDTH / 2, -WINDOW_HEIGHT / 2, 0f, // BOTTOM LEFT
        };

        float[] wallRightVertices = new float[] {
                WINDOW_WIDTH / 2 - wallSize, WINDOW_HEIGHT / 2, 0f, // TOP LEFT
                WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2, 0f, // TOP RIGHT
                WINDOW_WIDTH / 2, -WINDOW_HEIGHT / 2, 0f, // BOTTOM RIGHT
                WINDOW_WIDTH / 2 - wallSize, -WINDOW_HEIGHT / 2, 0f, // BOTTOM LEFT
        };


        float[] wallLeftVertices = new float[] {
                -WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2, 0f, // TOP LEFT
                -WINDOW_WIDTH / 2 + wallSize, WINDOW_HEIGHT / 2, 0f, // TOP RIGHT
                -WINDOW_WIDTH / 2 + wallSize, -WINDOW_HEIGHT / 2, 0f, // BOTTOM RIGHT
                -WINDOW_WIDTH / 2, -WINDOW_HEIGHT / 2, 0f, // BOTTOM LEFT
        };



        Model wallTopModel = new Model();
        wallTopModel.init(wallTopVertices, Model.INDICES, Model.TEXTURE_COORDINATES);

        Model wallBottomModel = new Model();
        wallBottomModel.init(wallBottomVertices, Model.INDICES, Model.TEXTURE_COORDINATES);

        Model wallRightModel = new Model();
        wallRightModel.init(wallRightVertices, Model.INDICES, Model.TEXTURE_COORDINATES);

        Model wallLeftModel = new Model();
        wallLeftModel.init(wallLeftVertices, Model.INDICES, Model.TEXTURE_COORDINATES);

        Texture texWhite = new Texture();
        texWhite.init(Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.WHITE);

        GameObject objWallTop = new StaticObject();
        objWallTop.init(wallTopModel, texWhite);

        GameObject objWallBottom = new StaticObject();
        objWallBottom.init(wallBottomModel, texWhite);

        GameObject objWallRight = new StaticObject();
        objWallRight.init(wallRightModel, texWhite);

        GameObject objWallLeft = new StaticObject();
        objWallLeft.init(wallLeftModel, texWhite);


        // TODO: only useful for client
        PlayerNumberMsgHandler playerNumberMsgHandler = new PlayerNumberMsgHandler();
        PlayerMsgHandler playerMsgHandler = new PlayerMsgHandler();

        NetManager.getInstance().init(ENetManagerType.CLIENT, null);

        NetManager.getInstance().register(playerNumberMsgHandler);
        NetManager.getInstance().register(playerMsgHandler);

        LoginMsg loginMsg = new LoginMsg();
        loginMsg.name = "Client";

        NetManager.getInstance().send(loginMsg);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

        System.out.println("Before while");
        while (playerNumber < 0) { }
        System.out.println("After while");

        synchronized (players) {
            players.put(playerNumber, player);
        }

        staticObjects.add(objWallTop);
        staticObjects.add(objWallBottom);
        staticObjects.add(objWallRight);
        staticObjects.add(objWallLeft);

        return true;
    }

    public boolean loop() {

        // FPS LIMITER
        float t0 = Time.getTime();
        float unprocessed = 0;

        float t1;
        float passed;

        boolean render;

        while ( !glfwWindowShouldClose(window.getId()) ) {
            render = false;

            t1 = Time.getTime();
            passed = t1 - t0;

            unprocessed += passed;
            t0 = t1;

            while (unprocessed >= FPS) {
                unprocessed -= FPS;
                render = true;

                List<Command> cmds = input.handle();

                for (Command cmd : cmds) {
                    cmd.execute(player);
                }

                // TODO: also hacky
                PlayerMsg playerMsg = new PlayerMsg();
                playerMsg.player = playerNumber;
                playerMsg.modelMat = player.getModelMat();

                NetManager.getInstance().send(playerMsg);

                glfwPollEvents();

                updatePlayerPos();
            }

            if (render) {

                // TODO: filtering needs to be done
                List<GameObject> toRender = new ArrayList<>();

                for (GameObject obj : staticObjects) {
                    toRender.add(obj);
                }

                for (Map.Entry<Integer, PlayerObject> entry : players.entrySet()) {
                    toRender.add(entry.getValue());
                }


                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                renderer.render(cam, toRender);

                glfwSwapBuffers(window.getId());
            }
        }

        return true;
    }

    public void updatePlayerPos() {

        List<PlayerMsg> msgs = new ArrayList<>(playerMsgs);

        synchronized (playerMsgs) {
            playerMsgs.clear();
        }

        for (PlayerMsg msg : msgs) {

            int playerNumber = msg.player;
            Matrix4f modelMat = msg.modelMat;

            System.out.println("Player pos update: " + playerNumber);

            if (playerNumber == this.playerNumber) return;

            synchronized (players) {
                PlayerObject player = players.getOrDefault(playerNumber, new PlayerObject());

                // TODO: hacky
                if (player.model == null) {

                    Model quad = new Model();
                    quad.init(Model.VERTICES, Model.INDICES, Model.TEXTURE_COORDINATES);

                    Texture texture = new Texture();
                    texture.init(Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.TEXTURE);

                    player.init(quad, texture);
                }

                player.model.getModelMat().identity().mul(modelMat);
                players.put(playerNumber, player);
            }
        }
    }

    public boolean exit() {

        window.exit();

        NetManager.getInstance().run = false;

        System.out.println("game.Game exited successfully");
        return true;
    }

}
