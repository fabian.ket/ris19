package game;

import graphics.Model;
import graphics.Texture;
import org.joml.Matrix4f;

public abstract class GameObject {

    protected Model model = null;
    protected Texture tex = null;

    public boolean init(float[] vertices, int[] indices, float[] tex_coordinates,
                        int tex_width, int tex_height, byte[] tex_data) {

        model = new Model();
        model.init(vertices, indices, tex_coordinates);

        tex = new Texture();
        tex.init(tex_width, tex_height, tex_data);

        return true;
    }

    public boolean init(Model m, Texture t) {
        model = m;
        tex = t;

        return true;
    }

    public boolean render() {

        if (tex == null || model == null) {
            return false;
        }

        tex.bind(0);
        model.render();

        return true;
    }

    public Matrix4f getModelMat() {
        return (model != null) ? model.getModelMat() : null;
    }
}
