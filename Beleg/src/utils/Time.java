package utils;

public class Time {

    public static float getTime() {
        return ((float)System.nanoTime()) / 1000000000L;
    }
}
