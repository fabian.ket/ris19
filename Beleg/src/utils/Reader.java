package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Reader {

    public static String readFile(String filename, String dir) throws IOException {
        StringBuilder string = new StringBuilder();
        BufferedReader br;

        br = new BufferedReader(new FileReader(new File("./" + dir + "/" + filename)));
        String line;

        while ((line = br.readLine()) != null) {
            string.append(line);
            string.append("\n");
        }

        return string.toString();
    }

    public static byte[] loadImage(String filename, String dir) {

        return null;
    }

}
