package graphics;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import java.nio.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public class Window {

    private static final class InstanceHolder {
        static final Window INSTANCE = new Window();
    }

    // The id handle
    private long id = -1;

    private int width = 800;
    private int height = 450;

    private String windowTitle = "Celda";

    private Window() {}

    public static Window getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void setSize(int w, int h) {
        width  = w;
        height = h;
    }

    public long getId() { return id; }

    public void exit() {
        // Free the id callbacks and destroy the id
        glfwFreeCallbacks(id);
        glfwDestroyWindow(id);

        // Terminate GLFW and free the error callback
        glfwTerminate();

        try {
            glfwSetErrorCallback(null).free();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

    }

    public void init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current id hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the id will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // the id will be resizable

        // Create the id
        id = glfwCreateWindow(width, height, windowTitle, NULL, NULL);
        if ( id == NULL )
            throw new RuntimeException("Failed to create the GLFW id");

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(id, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE ) {
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
            }
        });

        // Get the thread stack and push a new frame
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the id size passed to glfwCreateWindow
            glfwGetWindowSize(id, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the id
            glfwSetWindowPos(
                    id,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(id);

        // Enable v-sync
        glfwSwapInterval(1);

        // Make the id visible
        glfwShowWindow(id);

        GL.createCapabilities();

        // Set the clear color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        glEnable(GL_TEXTURE_2D);
    }
}
