package graphics;

import game.GameObject;
import org.joml.Matrix4f;

import java.util.ArrayList;
import java.util.List;

public class Renderer {

    // private List<GameObject> objs;
    private Shader shader = new Shader();


    public Renderer() {
        // objs = new ArrayList<>();
    }

    public void addGameObject(GameObject obj) {
        // objs.add(obj);
    }
    public void removeGameObject(GameObject obj) {
        // objs.remove(obj);
    }

    public boolean init(String shaderFile) {
        return shader.init(shaderFile);
    }

//    public boolean render(Camera cam) {
//
//
//        shader.bind();
//        shader.setUniform("sampler", 0);
//        shader.setUniform("projection", cam.getProjection());
//        shader.setUniform("view", new Matrix4f());
//
//
//        boolean result = true;
//
//        for (GameObject obj : objs) {
//
//            shader.setUniform("model", obj.getModelMat());
//
//            if (!obj.render()) {
//                result = false;
//                break;
//            }
//        }
//
//        return result;
//    }

    public boolean render(Camera cam, List<GameObject> objects) {

        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", cam.getProjection());
        shader.setUniform("view", new Matrix4f());

        boolean result = true;

        for (GameObject obj : objects) {

            shader.setUniform("model", obj.getModelMat());

            if (!obj.render()) {
                result = false;
                break;
            }
        }

        return result;
    }

}
