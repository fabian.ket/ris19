package graphics;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;

public class Model {

    private int vId;
    private int tId;
    private int iId;

    private int drawCount;
    private static final int DIMENSIONS = 3;

    private Matrix4f modelMat = new Matrix4f();



    // Default quad
    public static final float[] VERTICES = new float[]{
            -10f,  10f, 0f, // TOP LEFT
             10f,  10f, 0f, // TOP RIGHT
             10f, -10f, 0f, // BOTTOM RIGHT
            -10f, -10f, 0f, // BOTTOM LEFT
    };

    public static final float[] TEXTURE_COORDINATES = new float[] {
            0, 0, // TOP LEFT
            1, 0, // TOP RIGHT
            1, 1, // BOTTOM RIGHT
            0, 1, // BOTTOM LEFT
    };

    public static final int[] INDICES = new int[] {
            0, 1, 2,
            2, 3, 0
    };




    public boolean init(float[] vertices, int[] indices, float[] texCoordinates) {

        // drawCount = vertices.length / dimensions;
        drawCount = indices.length;

        vId = genBuffer(vertices, GL_ARRAY_BUFFER, GL_STATIC_DRAW);
        iId = genBuffer(indices, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW);
        tId = genBuffer(texCoordinates, GL_ARRAY_BUFFER, GL_STATIC_DRAW);

        return true;
    }

    public boolean render() {
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, vId);
        glVertexAttribPointer(0, DIMENSIONS, GL_FLOAT, false, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iId);

        glBindBuffer(GL_ARRAY_BUFFER, tId);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);


        glDrawElements(GL_TRIANGLES, drawCount, GL_UNSIGNED_INT, 0);


        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);


        return true;
    }

    public Matrix4f getModelMat() { return modelMat; }

    private FloatBuffer createFloatBuffer(float[] data) {
        FloatBuffer fBuffer = BufferUtils.createFloatBuffer(data.length);
        fBuffer.put(data);
        fBuffer.flip();

        return fBuffer;
    }

    private IntBuffer createIntBuffer(int[] data) {
        IntBuffer iBuffer = BufferUtils.createIntBuffer(data.length);
        iBuffer.put(data);
        iBuffer.flip();

        return iBuffer;
    }

    private int genBuffer(float[] data, int type, int usage) {
        int id = glGenBuffers();
        glBindBuffer(type, id);
        glBufferData(type, createFloatBuffer(data), usage);
        glBindBuffer(type, 0);

        return id;
    }

    private int genBuffer(int[] data, int type, int usage) {
        int id = glGenBuffers();
        glBindBuffer(type, id);
        glBufferData(type, createIntBuffer(data), usage);
        glBindBuffer(type, 0);

        return id;
    }
}
