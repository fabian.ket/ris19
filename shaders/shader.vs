#version 120

attribute vec3 vertices;
attribute vec2 texCoordsIn;

varying vec2 texCoordsOut;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main() {
    gl_Position = projection * view * model * vec4(vertices, 1);
    texCoordsOut = texCoordsIn;
}