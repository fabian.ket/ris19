package Client;

import Common.*;

public class Main {
    public static void main(String[] args) {
        NetManager netManager = NetManager.getInstance();

        if (!netManager.init(ENetManagerType.CLIENT, null)) {
            System.out.println("Could not connect!");
            return;
        }

        netManager.verbose = 1;

        ChatMsg msg = new ChatMsg();
        Vector2DMsg vec = new Vector2DMsg();
        vec.data = new Vector2D();

        int i = 0;
        while (true) {
            msg.data = "Hallo world! " + (++i);
            netManager.send(msg);

            vec.data.x = i;
            vec.data.y = i;

            netManager.send(vec);
        }
    }
}
