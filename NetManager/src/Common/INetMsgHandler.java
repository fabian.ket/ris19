package Common;

public interface INetMsgHandler<T extends NetMsg> {

    void handle(T msg);
    ENetMsgType type();
}
