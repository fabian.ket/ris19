package Common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;


public class NetManager {

    public static final int PORT = 12345;
    public int verbose = 0;

    private static class InstanceHolder {
        private static NetManager INSTANCE = new NetManager();
    }

    private Map<ENetMsgType, INetMsgHandler> handlers = new HashMap<>();

    private ServerSocket server;
    private List<Socket> sockets = new ArrayList<>();

    private BlockingDeque<NetMsg> msgs = new LinkedBlockingDeque<>();

    private Thread handlerThread;
    private Thread receiveThread;
    private Thread acceptThread;


    private final Object socketsLock = new Object();


    private NetManager() {}


    public static NetManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public boolean init(ENetManagerType type, String host) {
        boolean result;

        NetManager netManager = InstanceHolder.INSTANCE;

        if (type == ENetManagerType.SERVER) {
            result = netManager.initServer();
        }
        else {
            result = netManager.connectToServer(host);
        }

        if (!result) {
            return false;
        }

        netManager.receiveThread = new Thread(netManager::receive);
        netManager.receiveThread.start();

        netManager.handlerThread = new Thread(netManager::handle);
        netManager.handlerThread.start();

        return true;
    }

    private boolean initServer() {
        boolean result;

        try {
            server = new ServerSocket(PORT);
            result = true;

            acceptThread = new Thread(this::accept);
            acceptThread.start();

        } catch (IOException io) {
            io.printStackTrace();
            result = false;
        }
        return result;
    }

    private boolean connectToServer(String ip) {
        boolean result;

        try {
            Socket server = new Socket(ip, PORT);

            synchronized (socketsLock) {
                sockets.add(server);
            }

            result = true;
        } catch (IOException io) {
            io.printStackTrace();
            result = false;
        }

        return result;
    }

    private void accept() {
        while (true) {
            try {
                Socket client = server.accept();

                synchronized (socketsLock) {
                    sockets.add(client);
                }

                if (verbose > 0) {
                    System.out.println("Client connected!");
                }

            } catch (IOException io) {
                io.printStackTrace();
                break;
            }
        }
    }

    public void send(NetMsg msg) {
        synchronized (socketsLock) {
            for (Socket socket : sockets) {
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeUnshared(msg);
                    oos.flush();

                    if (verbose > 0) {
                        System.out.println("Msg send!");
                    }
                } catch (IOException io) {
                    io.printStackTrace();
                }
            }
        }
    }

    private void receive() {
        while (true) {
            synchronized (socketsLock) {
                for (Socket socket : sockets) {
                    try {
                        if (socket.getInputStream().available() > 0) {

                            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                            NetMsg msg = (NetMsg) ois.readObject();
                            msgs.offer(msg);

                            if (verbose > 0) {
                                System.out.println("New msg added!");
                            }
                        }
                    } catch (IOException io) {
                        io.printStackTrace();
                        break;
                    } catch (ClassNotFoundException cnfe) {
                        cnfe.printStackTrace();
                    }
                }
            }

            try {
                Thread.sleep(16);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    public void register(INetMsgHandler<? extends NetMsg> handler) {
        handlers.put(handler.type(), handler);
    }

    private void handle() {
        while (true) {
            try {
                NetMsg msg = msgs.take();

                @SuppressWarnings("unchecked")
                INetMsgHandler<NetMsg> handler = handlers.get(msg.type());

                if (handler != null) {
                    // TODO: java generic type stuff
                    handler.handle(msg);
                } else {
                    msgs.offer(msg);
                }

            } catch (InterruptedException ie) {
                ie.printStackTrace();
                break;
            }
        }
    }
}
