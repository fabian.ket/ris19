package Common;

import java.io.Serializable;

public abstract class NetMsg implements Serializable {

    public abstract ENetMsgType type();
}
