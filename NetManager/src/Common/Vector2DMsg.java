package Common;

public class Vector2DMsg extends NetMsg {

    public Vector2D data;

    @Override
    public ENetMsgType type() {
        return ENetMsgType.VECTOR2D;
    }
}
