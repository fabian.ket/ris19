package Server;

import Common.ENetMsgType;
import Common.INetMsgHandler;
import Common.Vector2DMsg;

public class Vector2DHandler implements INetMsgHandler<Vector2DMsg> {
    @Override
    public void handle(Vector2DMsg msg) {
        System.out.println("(" + msg.data.x + ", " + msg.data.y + ")");
    }

    @Override
    public ENetMsgType type() {
        return ENetMsgType.VECTOR2D;
    }
}
