package Server;

import Common.ChatMsg;
import Common.ENetMsgType;
import Common.INetMsgHandler;

public class ChatMsgHandler implements INetMsgHandler<ChatMsg> {

    @Override
    public ENetMsgType type() {
        return ENetMsgType.CHAT;
    }

    @Override
    public void handle(ChatMsg msg) {
        System.out.println(msg.data);
        System.out.println("Handled Common.ChatMsg");
    }
}
