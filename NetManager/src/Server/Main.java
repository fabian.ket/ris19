package Server;

import Common.ENetManagerType;
import Common.NetManager;


public class Main {

    public static void main(String[] args) {

        NetManager netManager = NetManager.getInstance();

        if (!netManager.init(ENetManagerType.SERVER, null)) {
            System.out.println("Could not connect!");
            return;
        }

        netManager.verbose = 1;

        ChatMsgHandler chatHandler = new ChatMsgHandler();
        Vector2DHandler vector2DHandler = new Vector2DHandler();

        netManager.register(chatHandler);
        netManager.register(vector2DHandler);
    }
}
