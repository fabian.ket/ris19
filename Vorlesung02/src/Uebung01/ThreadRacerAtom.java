package Uebung01;

public class ThreadRacerAtom {


    private static Thread t1;
    private static Thread t2;
    private static Thread t3;
    private static Thread t4;

    private static volatile int iThreads = 0;
    private static final int MAX_THREADS = 4;

    private static int prints = 100;

    private static Object waitThreads = new Object();

    public static void main(String[] args) {



        t1 = new Thread(() -> {
            print(t1.getId());
        });

        t2 = new Thread(() -> {
            print(t2.getId());
        });

        t3 = new Thread(() -> {
            print(t3.getId());
        });

        t4 = new Thread(() -> {
            print(t4.getId());
        });


        t1.start();
        t2.start();
        t3.start();
        t4.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public static void print(long id) {

        for (int i = 1; i <= prints; i++) {
            System.out.println(String.format("Thread %d: %d", id, i));

            if (i % 20 == 0) {

                System.out.println("=========================");

                synchronized (waitThreads) {

                    iThreads++;

                    if (iThreads >= MAX_THREADS) {

                        iThreads = 0;
                        waitThreads.notifyAll();
                    }
                    else {
                        try {
                            waitThreads.wait();
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
