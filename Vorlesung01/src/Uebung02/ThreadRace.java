package Uebung02;

public class ThreadRace {

    private static Thread t1;
    private static Thread t2;
    private static Thread t3;
    private static Thread t4;

    private static int prints = 10;

    public static void main(String[] args) {



        t1 = new Thread(() -> {
            for (int i = 0; i < prints; i++) {
                System.out.println(String.format("Thread %d: %d", t1.getId(), i));
            }
        });

        t2 = new Thread(() -> {
            for (int i = 0; i < prints; i++) {
                System.out.println(String.format("Thread %d: %d", t2.getId(), i));
            }
        });

        t3 = new Thread(() -> {
            for (int i = 0; i < prints; i++) {
                System.out.println(String.format("Thread %d: %d", t3.getId(), i));
            }
        });

        t4 = new Thread(() -> {
            for (int i = 0; i < prints; i++) {
                System.out.println(String.format("Thread %d: %d", t4.getId(), i));
            }
        });


        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
