package Uebung01.Client;

import java.io.IOException;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        int port = 12345;
        int data;


        try (Socket server = new Socket("", port)) {

            for (int i = 0; i < 10; i++) {
                server.getOutputStream().write(i);

                data = server.getInputStream().read();
                System.out.println(String.format("Client received: %d", data));
            }

        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
