package Uebung01.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {

        int port = 12345;
        int data;

        try (ServerSocket serverSocket = new ServerSocket(port)) {
            Socket client = serverSocket.accept();

            for (int i = 9; i >= 0; i--) {
                data = client.getInputStream().read();
                System.out.println(String.format("Server received: %d", data));

                client.getOutputStream().write(i);
            }

            client.close();

        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
