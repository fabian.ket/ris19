package Utils;

import org.lwjgl.BufferUtils;
import org.lwjgl.stb.STBImage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class Reader {

    public static String readFile(String filename, String dir) throws IOException {
        StringBuilder string = new StringBuilder();
        BufferedReader br;

        br = new BufferedReader(new FileReader(new File("./" + dir + "/" + filename)));
        String line;

        while ((line = br.readLine()) != null) {
            string.append(line);
            string.append("\n");
        }

        return string.toString();
    }

    public static class ImageReturn {
        public byte[] data = null;
        public int width  = 0;
        public int height = 0;
        public int depth  = 0;
    }

    public static ImageReturn loadImage(String filename) {

        IntBuffer width  = BufferUtils.createIntBuffer(1);
        IntBuffer height = BufferUtils.createIntBuffer(1);
        IntBuffer depth  = BufferUtils.createIntBuffer(1);

        ByteBuffer data  = STBImage.stbi_load(filename, width, height, depth, 4);

        ImageReturn ir = new ImageReturn();

        if (data != null)
            ir.data = data.array();

        ir.width  = width.get(0);
        ir.height = height.get(0);
        ir.depth  = depth.get(0);

        return ir;
    }

}
