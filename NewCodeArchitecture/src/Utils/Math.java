package Utils;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class Math {

    public static final double HALF_PI = java.lang.Math.PI / 2.0;

    public static Vector3f mul(Vector3f v, Matrix4f m) {
        Vector4f t = new Vector4f();
        t.x = v.x;
        t.y = v.y;
        t.z = v.z;
        t.w = 1;

        t.mul(m);

        return new Vector3f(t.x, t.y, t.z);
    }

    public static float fmap(float v, float min, float max) { return (max - min) * v + min; }
}
