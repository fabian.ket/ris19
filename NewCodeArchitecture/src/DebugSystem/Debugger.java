package DebugSystem;

import Msg.Msg;
import MsgHandler.Handler;

public class Debugger extends Handler<Msg> {

    @Override
    protected void handle(Msg msg) {

        switch (msg.type()) {
            case TEST: {

            } break;

            default: {
                System.err.println(String.format("Unknown msg type %s for %s!", msg, this));
            }
        }

    }

    @Override
    public String toString() {
        return "Debugger";
    }
}
