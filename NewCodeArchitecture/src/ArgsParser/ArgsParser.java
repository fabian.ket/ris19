package ArgsParser;

public class ArgsParser {

    public static Arguments parse(String[] args) {
        Arguments result = new Arguments();

        if (args.length > 0) {
            if (args[0].toLowerCase().equals("null")) {
                result.host = null;
            } else {
                result.host = args[0];
            }
        }

        if (args.length > 1) {
            result.port = Integer.parseInt(args[1]);
        }

        if (args.length > 2) {
            result.width = Integer.parseInt(args[2]);
        }

        if (args.length > 3) {
            result.height = Integer.parseInt(args[3]);
        }

        return result;
    }
}
