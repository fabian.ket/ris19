package Msg;

import org.joml.Matrix4f;

public class PlayerMsg extends Msg {

    public int playerNumber;
    public Matrix4f modelMat;
    public boolean hitByEnemy;

    public PlayerMsg(int pn, Matrix4f mat, boolean hit) {
        playerNumber = pn;
        modelMat = mat;
        hitByEnemy = hit;
    }

    @Override
    public EMsgType type() {
        return EMsgType.PLAYER;
    }
}
