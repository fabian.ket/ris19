package Msg;

import java.util.Random;

public class EventBackgroundColorMsg extends Msg {

    private static final Random rand = new Random(System.currentTimeMillis());

    public float r;
    public float g;
    public float b;

    public EventBackgroundColorMsg() {
        r = rand.nextFloat() * 0.5f;
        g = rand.nextFloat() * 0.5f;
        b = rand.nextFloat() * 0.5f;
    }

    public EventBackgroundColorMsg(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    @Override
    public EMsgType type() {
        return EMsgType.EVENT_BACKGROUND_COLOR;
    }
}
