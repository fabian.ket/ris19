package Msg;

import org.joml.Vector3f;

public class PlayerShotMsg extends Msg {

    public int playerNumber;
    public Vector3f direction;

    public PlayerShotMsg(int n, Vector3f v) {
        playerNumber = n;
        direction = v;
    }

    @Override
    public EMsgType type() { return EMsgType.PLAYER_SHOT; }
}
