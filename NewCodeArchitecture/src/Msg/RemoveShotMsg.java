package Msg;

public class RemoveShotMsg extends Msg {

    public int id;

    public RemoveShotMsg(int id) { this.id = id; }

    @Override
    public EMsgType type() { return EMsgType.REMOVE_SHOT; }
}
