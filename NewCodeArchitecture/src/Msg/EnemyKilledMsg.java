package Msg;

public class EnemyKilledMsg extends Msg {

    public int id;

    public EnemyKilledMsg(int id) {
        this.id = id;
    }

    @Override
    public EMsgType type() {
        return EMsgType.ENEMY_KILLED;
    }
}
