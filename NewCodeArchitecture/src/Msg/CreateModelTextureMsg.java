package Msg;

import org.joml.Matrix4f;

public class CreateModelTextureMsg extends Msg {

    public int id = -1;

    public float[] vertices;
    public int[] indices;
    public float[] textureCoordinates;

    public int width1;
    public int height1;
    public byte[] data1;

    public int width2;
    public int height2;
    public byte[] data2;

    public Matrix4f m;

    public CreateModelTextureMsg(int id, float[] vertices, int[] indices, float[] textureCoordinates,
                                 int width1, int height1, byte[] data1,
                                 int width2, int height2, byte[] data2,
                                 Matrix4f m) {
        this.id = id;

        this.vertices = vertices;
        this.indices  = indices;
        this.textureCoordinates = textureCoordinates;

        this.width1  = width1;
        this.height1 = height1;
        this.data1 = data1;

        this.width2  = width2;
        this.height2 = height2;
        this.data2 = data2;

        this.m = m;
    }

    @Override
    public EMsgType type() {
        return EMsgType.CREATE_MODEL_TEXTURE;
    }
}
