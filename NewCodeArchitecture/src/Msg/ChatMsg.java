package Msg;

public class ChatMsg extends Msg {

    public String msg;
    public ChatMsg(String m) { msg = m; }
    public ChatMsg(SendChatMsg m) { msg = m.msg; }

    public EMsgType type() {
        return EMsgType.CHAT;
    }
}
