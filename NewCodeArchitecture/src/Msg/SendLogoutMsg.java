package Msg;

public class SendLogoutMsg extends Msg {

    public int playerNumber;

    public SendLogoutMsg(int pn) { playerNumber = pn; }

    @Override
    public EMsgType type() { return EMsgType.SEND_LOGOUT; }
}
