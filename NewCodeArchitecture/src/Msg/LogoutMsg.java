package Msg;

public class LogoutMsg extends Msg {

    public int playerNumber;

    public LogoutMsg(int pn) { playerNumber = pn; }

    @Override
    public EMsgType type() { return EMsgType.LOGOUT; }
}
