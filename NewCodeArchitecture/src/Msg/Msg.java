package Msg;

import java.io.Serializable;

public abstract class Msg implements Serializable {

    public abstract EMsgType type();
}
