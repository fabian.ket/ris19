package Msg;

public class InitGameMsg extends Msg {
    public int playerNumber;
    public long seed;

    public InitGameMsg(int pn, long s) { playerNumber = pn; seed = s; }

    @Override
    public EMsgType type() {
        return EMsgType.INIT_GAME;
    }
}
