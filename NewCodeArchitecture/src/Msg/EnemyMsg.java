package Msg;

import org.joml.Matrix4f;

public class EnemyMsg extends Msg {

    public int id;
    public Matrix4f modelMat;

    public EnemyMsg(int id, Matrix4f modelMat) {
        this.id = id;
        this.modelMat = modelMat;
    }

    @Override
    public EMsgType type() {
        return EMsgType.ENEMY;
    }
}
