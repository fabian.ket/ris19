package Msg;

import GraphicSystem.Model;
import GraphicSystem.Texture;

public class ModelTextureCreatedMsg extends Msg {

    public int id;
    public Model m;
    public Texture t1;
    public Texture t2;

    public ModelTextureCreatedMsg(int id, Model m, Texture t1, Texture t2) {
        this.id  = id;
        this.m   = m;
        this.t1  = t1;
        this.t2  = t2;
    }

    @Override
    public EMsgType type() {
        return EMsgType.MODEL_TEXTURE_CREATED;
    }
}
