package Msg;

public class DebugMsg extends Msg {

    @Override
    public EMsgType type() {
        return EMsgType.DEBUG;
    }
}
