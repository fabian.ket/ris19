package Msg;

import org.joml.Matrix4f;

public class ShotMsg extends Msg {

    public int id;
    public Matrix4f modelMat;
    public float rotationAngle;

    public ShotMsg(int id, Matrix4f modelMat, float a) {
        this.id = id;
        this.modelMat  = modelMat;
        this.rotationAngle = a;
    }

    @Override
    public EMsgType type() {
        return EMsgType.SHOT;
    }
}
