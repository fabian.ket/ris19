package Msg;

public class SendChatMsg extends Msg {

    public String msg;
    public SendChatMsg(String m) { msg = m; }

    @Override
    public EMsgType type() {
        return EMsgType.SEND_CHAT;
    }
}
