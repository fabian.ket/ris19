package Msg;
import org.joml.Vector3f;

public class SendPlayerMsg extends Msg {

    public int playerNumber;
    public Vector3f direction;

    public SendPlayerMsg(int pn, Vector3f v) {
        playerNumber = pn;
        direction = v;
    }

    @Override
    public EMsgType type() {
        return EMsgType.SEND_PLAYER;
    }
}
