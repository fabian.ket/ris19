package Msg;

import NetworkSystem.ClientHandler;

public class LoginMsg extends Msg {

    public int playerNumber;
    public ClientHandler clientHandler;

    public LoginMsg(int pn, ClientHandler ch) {
        playerNumber = pn;
        clientHandler = ch;
    }

    @Override
    public EMsgType type() {
        return EMsgType.LOGIN;
    }
}
