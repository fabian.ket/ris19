package Msg;

public class StopMsg extends Msg {

    @Override
    public EMsgType type() {
        return EMsgType.STOP;
    }
}
