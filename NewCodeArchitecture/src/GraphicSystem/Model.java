package GraphicSystem;

import Game.Collider;
import Game.GameObject;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL32.GL_LINES_ADJACENCY;
import static org.lwjgl.opengl.GL40.GL_PATCHES;

public class Model {

    private int vId;
    private int tId;
    private int iId;

    private boolean renderable = false;

    private int drawCount;
    private static final int DIMENSIONS = 3;

    private Matrix4f modelMat = new Matrix4f();



    // Default quad
    public static final float[] VERTICES = new float[]{
            -10f,  10f, 0f, // TOP LEFT
             10f,  10f, 0f, // TOP RIGHT
             10f, -10f, 0f, // BOTTOM RIGHT
            -10f, -10f, 0f, // BOTTOM LEFT
    };

    public static final Collider COLLIDER = new Collider(new Vector3f(0,0,0), GameObject.COLLIDER_RADIUS);

    public static final float[] TEXTURE_COORDINATES = new float[] {
            0, 0, // TOP LEFT
            1, 0, // TOP RIGHT
            1, 1, // BOTTOM RIGHT
            0, 1, // BOTTOM LEFT
    };

    public static final int[] INDICES = new int[] {
            0, 1, 2,
            2, 3, 0
    };


    public static final float[] CIRCLE = new float[]{
            -3,  9, 0, // TOP LEFT
             3,  9, 0, // TOP RIGHT
             9,  3, 0, // CENTER TOP RIGHT
             9, -3, 0, // CENTER BOTTOM RIGHT
             3, -9, 0, // BOTTOM RIGHT
            -3, -9, 0, // BOTTOM LEFT
            -9, -3, 0, // CENTER BOTTOM LEFT
            -9,  3, 0  // CENTER TOP LEFT
    };

    public static final int[] CIRCLE_INDICES = new int[] {
//            0, 1, 2,
//            0, 2, 3,
//            0, 3, 4,
//            0, 4, 5,
//            0, 5, 6,
//            0, 6, 7
            0, 1, 2, 3, 4, 5, 6, 7
    };

    public static final float[] CIRCLE_TEXTURE = new float[] {
            0.3333f, 0, // TOP LEFT
            0.6666f, 0, // TOP RIGHT
            1, 0.3333f, // CENTER TOP RIGHT
            1, 0.6666f, // CENTER BOTTOM RIGHT
            0.6666f, 1, // BOTTOM RIGHT
            0.3333f, 1, // BOTTOM LEFT
            0, 0.6666f, // CENTER BOTTOM LEFT
            0, 0.3333f  // CENTER TOP LEFT
    };


    public static final float[] TRIANGLE = new float[]{
             0f,  6f, 0, // TOP CENTER
             6f, -6f, 0, // RIGHT BOTTOM
            -6f, -6f, 0  // LEFT BOTTOM
    };

    public static final int[] TRIANGLE_INDICES = new int[] {
            0, 1, 2
    };

    public static final float[] TRIANGLE_TEXTURE = new float[] {
            0.5f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f
    };


    public Model() {}
    public Model(float[] vertices, int[] indices, float[] textureCoordinates) {
        init(vertices, indices, textureCoordinates);
    }


    public boolean init(float[] vertices, int[] indices, float[] texCoordinates) {

        // drawCount = vertices.length / dimensions;
        drawCount = indices.length;

        vId = genBuffer(vertices, GL_ARRAY_BUFFER, GL_STATIC_DRAW);
        iId = genBuffer(indices, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW);
        tId = genBuffer(texCoordinates, GL_ARRAY_BUFFER, GL_STATIC_DRAW);

        renderable = true;

        return true;
    }

    public boolean render(int mode) {
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, vId);
        glVertexAttribPointer(0, DIMENSIONS, GL_FLOAT, false, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iId);

        glBindBuffer(GL_ARRAY_BUFFER, tId);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);


        glDrawElements(mode , drawCount, GL_UNSIGNED_INT, 0);


        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);


        return true;
    }

    public boolean getRenderable() { return renderable; }

    public Matrix4f getModelMat() { return modelMat; }
    public void setModelMat(Matrix4f mat) { modelMat = mat; }

    private FloatBuffer createFloatBuffer(float[] data) {
        FloatBuffer fBuffer = BufferUtils.createFloatBuffer(data.length);
        fBuffer.put(data);
        fBuffer.flip();

        return fBuffer;
    }

    private IntBuffer createIntBuffer(int[] data) {
        IntBuffer iBuffer = BufferUtils.createIntBuffer(data.length);
        iBuffer.put(data);
        iBuffer.flip();

        return iBuffer;
    }

    private int genBuffer(float[] data, int type, int usage) {
        int id = glGenBuffers();
        glBindBuffer(type, id);
        glBufferData(type, createFloatBuffer(data), usage);
        glBindBuffer(type, 0);

        return id;
    }

    private int genBuffer(int[] data, int type, int usage) {
        int id = glGenBuffers();
        glBindBuffer(type, id);
        glBufferData(type, createIntBuffer(data), usage);
        glBindBuffer(type, 0);

        return id;
    }
}
