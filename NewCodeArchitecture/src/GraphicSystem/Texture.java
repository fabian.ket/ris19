package GraphicSystem;

import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

public class Texture {

    private int id;
    private int width;
    private int height;
    private boolean renderable = false;


    // Default texture
    public static final int TEXTURE_WIDTH  = 2;
    public static final int TEXTURE_HEIGHT = 2;

    public static final byte[] COLORFUL = new byte[]{
            //       R         G           B         A   |          R           G           B         A
            (byte) 255, (byte) 0, (byte)   0, (byte) 0,    (byte)   0, (byte) 255, (byte)   0, (byte) 0,
            (byte)   0, (byte) 0, (byte) 255, (byte) 0,    (byte) 255, (byte) 255, (byte) 255, (byte) 0
    };

    public static final byte[] RED = new byte[]{
            //       R           G           B         A   |          R           G           B         A
            (byte) 255, (byte)   0, (byte)   0, (byte) 0,    (byte) 255, (byte)   0, (byte)   0, (byte) 0,
            (byte) 255, (byte)   0, (byte)   0, (byte) 0,    (byte) 255, (byte)   0, (byte)   0, (byte) 0
    };

    public static final byte[] GREEN = new byte[]{
            //       R         G           B         A     |          R           G           B         A
            (byte)   0, (byte) 255, (byte)   0, (byte) 0,    (byte)   0, (byte) 255, (byte)   0, (byte) 0,
            (byte)   0, (byte) 255, (byte)   0, (byte) 0,    (byte)   0, (byte) 255, (byte)   0, (byte) 0
    };

    public static final byte[] BLUE = new byte[]{
            //       R         G           B         A   |          R           G           B         A
            (byte)   0, (byte) 0, (byte) 255, (byte) 0,    (byte)   0, (byte)   0, (byte) 255, (byte) 0,
            (byte)   0, (byte) 0, (byte) 255, (byte) 0,    (byte)   0, (byte)   0, (byte) 255, (byte) 0
    };

    public static final byte[] BLACK = new byte[]{
            //       R         G           B         A   |          R           G           B         A
            (byte)   0, (byte) 0, (byte)   0, (byte) 0,    (byte)   0, (byte)   0, (byte)   0, (byte) 0,
            (byte)   0, (byte) 0, (byte)   0, (byte) 0,    (byte)   0, (byte)   0, (byte)   0, (byte) 0
    };

    public static final byte[] WHITE = new byte[]{
            //       R         G           B         A   |          R           G           B         A
            (byte) 255, (byte) 255, (byte) 255, (byte) 0,    (byte) 255, (byte) 255, (byte) 255, (byte) 0,
            (byte) 255, (byte) 255, (byte) 255, (byte) 0,    (byte) 255, (byte) 255, (byte) 255, (byte) 0
    };

    public static final byte[] YELLOW = new byte[]{
            //       R           G           B         A   |          R           G           B         A
            (byte) 255, (byte) 255, (byte)   0, (byte) 0,    (byte) 255, (byte) 255, (byte)   0, (byte) 0,
            (byte) 255, (byte) 255, (byte)   0, (byte) 0,    (byte) 255, (byte) 255, (byte)   0, (byte) 0
    };

    public static final byte[] GRAY = new byte[]{
            //       R         G           B         A   |          R           G           B         A
            (byte) 128, (byte) 128, (byte) 128, (byte) 0,    (byte) 128, (byte) 128, (byte) 128, (byte) 0,
            (byte) 128, (byte) 128, (byte) 128, (byte) 0,    (byte) 128, (byte) 128, (byte) 128, (byte) 0
    };


    public Texture() {}
    public Texture(int width, int height, byte[] data) {
        init(width, height, data);
    }


    public boolean init(int w, int h, byte[] data) {

        width  = w;
        height = h;


        ByteBuffer bBuffer = BufferUtils.createByteBuffer(width * height * 4);
        bBuffer.put(data);
        bBuffer.flip();

        id = glGenTextures();

        glBindTexture(GL_TEXTURE_2D, id);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bBuffer);

        glBindTexture(GL_TEXTURE_2D, 0);

        renderable = true;

        return true;
    }

    public boolean getRenderable() { return renderable; }

    public boolean bind(int sampler) {

        if (sampler < 0 || sampler > 31) {
            return false;
        }

        glActiveTexture(GL_TEXTURE0 + sampler);
        glBindTexture(GL_TEXTURE_2D, id);
        return true;
    }
}
