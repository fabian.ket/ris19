package GraphicSystem;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Camera {

    private Vector3f position;
    private Matrix4f projection;

    public boolean init(int width, int height) {
        int halfWidth  = width  >>> 1;
        int halfHeight = height >>> 1;

        position = new Vector3f(0,0,0);
        projection = new Matrix4f().ortho2D(-halfWidth, halfWidth, -halfHeight, halfHeight);

        return true;
    }

    public void setPosition(Vector3f pos) { position = pos; }
    public void addposition(Vector3f pos) { position.add(pos); }

    public Vector3f getPosition() { return position; }

    public Matrix4f getProjection() {
        Matrix4f res = new Matrix4f();
        Matrix4f pos = new Matrix4f().setTranslation(position);

        res = projection.mul(pos, res);

        return res;
    }

}
