package GraphicSystem;

import Game.Game;
import Game.GameObject;
import Msg.*;
import MsgHandler.Handler;
import MsgHandler.IMsgHandler;
import Utils.Time;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

// Singleton
public class Renderer extends Handler<Msg> {

    private static class InstanceHolder {
        private static final Renderer INSTANCE = new Renderer();
    }

    // render members
    private long windowId = -1;
    private String windowTitle = "GorN";

    private float deltaTime = 0.f;

    private int width = 800;
    private int height = 450;
    private String shaderFile = "shader";

    private Camera cam = new Camera();
    private Shader shader = new Shader();

    private Lock renderLock = new ReentrantLock();
    private List<GameObject> toRender = new ArrayList<>();

    private Renderer() { }

    public static Renderer getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public long getWindowId() {
        return windowId;
    }
    public float getDeltaTime() { return deltaTime; }


    public void init(int w, int h, String shaderFile) {
        setSize(w, h);
        this.shaderFile = shaderFile;

        register(new IMsgHandler<CreateModelTextureMsg>() {
            @Override
            public void handle(CreateModelTextureMsg msg) {
                Model m = new Model(msg.vertices, msg.indices, msg.textureCoordinates);
                Texture t1 = new Texture(msg.width1, msg.height1, msg.data1);
                Texture t2 = new Texture(msg.width2, msg.height2, msg.data2);

                m.setModelMat(msg.m);

                ModelTextureCreatedMsg mtcm = new ModelTextureCreatedMsg(msg.id, m, t1, t2);

                try {
                    Game.getInstance().blockingPut(mtcm);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public EMsgType type() { return EMsgType.CREATE_MODEL_TEXTURE; }
        });

        register(new IMsgHandler<EventBackgroundColorMsg>() {
            @Override
            public void handle(EventBackgroundColorMsg msg) {
                // glClearColor(msg.r, msg.g, msg.b, 0.f);
                glClearColor(0, 0, 0, 0.f);
            }

            @Override
            public EMsgType type() { return EMsgType.EVENT_BACKGROUND_COLOR; }
        });

        Thread worker = new Thread(this::run);
        worker.start();
    }

    private void init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current windowId hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the windowId will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // the windowId will be resizable

        // Create the windowId
        windowId = glfwCreateWindow(width, height, windowTitle, NULL, NULL);
        if ( windowId == NULL )
            throw new RuntimeException("Failed to create the GLFW windowId");

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
//        glfwSetKeyCallback(windowId, (window, key, scancode, action, mods) -> {
//            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE ) {
//                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
//            }
//        });

        // Get the thread stack and push a new frame
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the windowId size passed to glfwCreateWindow
            glfwGetWindowSize(windowId, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the windowId
            glfwSetWindowPos(
                    windowId,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(windowId);

        // Enable v-sync
        glfwSwapInterval(1);

        // Make the windowId visible
        glfwShowWindow(windowId);

        GL.createCapabilities();

        // Set the clear color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        glEnable(GL_TEXTURE_2D);

        glPointSize(4f);

        // member stuff
        cam.init(width, height);
        shader.init(shaderFile);
    }

    public void exit() {
        // Free the windowId callbacks and destroy the windowId
        glfwFreeCallbacks(windowId);
        glfwDestroyWindow(windowId);

        // Terminate GLFW and free the error callback
        glfwTerminate();

        try {
            glfwSetErrorCallback(null).free();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        Game.getInstance().synchronizeExit();
    }

    private void setSize(int w, int h) {
        width  = w;
        height = h;
    }

    public void setRenderObjects(List<GameObject> objs) {
        renderLock.lock();
        toRender = objs;
        renderLock.unlock();
    }

    private void run() {

        init();
        Game.getInstance().synchronizeInit();

        float t0;
        while (Game.getInstance().getRun()) {

            t0 = Time.getTime();

            {
                glfwPollEvents();

                processMsgs();

                render();
            }

            Game.getInstance().synchronizeLoop();

            deltaTime = Time.getTime() - t0;
        }

        closeWindow();
        exit();
    }

    private void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", cam.getProjection());
        shader.setUniform("view", new Matrix4f());

        renderLock.lock();

        for (GameObject obj : toRender) {
            if (obj.getModelMat() != null) {
                shader.setUniform("model", obj.getModelMat());
                obj.render();
            }
        }

        renderLock.unlock();

        glfwSwapBuffers(windowId);
    }

    public void setCameraPosition(Vector3f v) {
        cam.setPosition(v);
    }

    private void closeWindow() {
        glfwSetWindowShouldClose(windowId, true);
    }

    private void processMsgs() {

        int size = size();

        for (int i = 0; i < size; i++) {

            try {
                Msg msg = blockingTake();
                handle(msg);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}
