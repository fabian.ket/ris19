package GraphicSystem;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import Utils.Reader;

import java.io.IOException;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

public class Shader {

    private int program;
    private int vertexShader;   // vertices shader
    private int fragmentShader; // color shader


    public boolean init(String filename) {

        program = glCreateProgram();

        vertexShader = glCreateShader(GL_VERTEX_SHADER);

        String shader = "";
        try {
            shader = Reader.readFile(filename + ".vs", "shaders");
        } catch (IOException io) {
            io.printStackTrace();
        }

        glShaderSource(vertexShader, shader);
        glCompileShader(vertexShader);

        if (glGetShaderi(vertexShader, GL_COMPILE_STATUS) != 1) {
            System.err.println(glGetShaderInfoLog(vertexShader));
            return false;
        }

        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        shader = "";
        try {
            shader = Reader.readFile(filename + ".fs", "shaders");
        } catch (IOException io) {
            io.printStackTrace();
        }

        glShaderSource(fragmentShader, shader);
        glCompileShader(fragmentShader);

        if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) != 1) {
            System.err.println(glGetShaderInfoLog(fragmentShader));
            return false;
        }


        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);

        glBindAttribLocation(program, 0, "vertices");
        glBindAttribLocation(program, 1, "texCoordsIn");


        glLinkProgram(program);
        if (glGetProgrami(program, GL_LINK_STATUS) != 1) {
            System.err.println(glGetProgramInfoLog(program));
            return false;
        }

        glValidateProgram(program);
        if (glGetProgrami(program, GL_VALIDATE_STATUS) != 1) {
            System.err.println(glGetProgramInfoLog(program));
            return false;
        }

        return true;
    }

    public boolean setUniform(String name, int value) {
        int location = glGetUniformLocation(program, name);

        if (location == -1) {
            return false;
        }

        glUniform1i(location, value);
        return true;
    }

    public boolean setUniform(String name, Matrix4f value) {
        int location = glGetUniformLocation(program, name);

        FloatBuffer fBuffer = BufferUtils.createFloatBuffer(16);

        value.get(fBuffer);

        if (location == -1) {
            return false;
        }

        glUniformMatrix4fv(location, false, fBuffer);

        return true;
    }

    public boolean bind() {
        glUseProgram(program);
        return true;
    }
}
