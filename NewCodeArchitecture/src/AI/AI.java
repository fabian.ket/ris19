package AI;

import Game.Collider;
import Game.GameObject;
import Game.PlayerObject;
import Utils.Math;
import org.joml.Vector3f;

import java.util.List;
import java.util.Map;

public class AI {

    public static final float AGGRO_RANGE = 300f;

    public Vector3f getDirectionToNearestPlayer(GameObject g, Map<Integer, PlayerObject> players) {

        Vector3f direction = new Vector3f();

        Vector3f gCenter = g.getCurrentCenterPosition();
        Vector3f pCenter;

        float distance = Float.MAX_VALUE;
        float t;

        int pId = -1;

        for (PlayerObject p : players.values()) {
            pCenter = Math.mul(p.getCenter(), p.getModelMat());

            t = gCenter.distance(pCenter);

            if (t < distance && t <= AGGRO_RANGE) {
                distance = t;

                pId = p.getId();
            }
        }

        if (pId != -1) {
            PlayerObject p = players.get(pId);
            pCenter = Math.mul(p.getCenter(), p.getModelMat());
            direction = new Vector3f(pCenter.x - gCenter.x, pCenter.y - gCenter.y, 0);
        }

        return direction;
    }

    public Collider getNearestObstacle(GameObject g, List<Collider> obstacles) {
        Vector3f pos = g.getCurrentPositionInSpace();
        Collider nearest = null;

        float distance = Float.MAX_VALUE;
        float currentDistance;

        for (Collider c : obstacles) {

            currentDistance = pos.distance(c.getCurrentPositionInSpace());

            if (currentDistance < distance) {
                distance = currentDistance;
                nearest = c;
            }
        }

        return nearest;
    }
}
