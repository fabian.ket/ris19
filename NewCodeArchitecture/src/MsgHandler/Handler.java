package MsgHandler;

import Msg.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class Handler<T extends Msg> {

    private BlockingDeque<T> msgs = new LinkedBlockingDeque<>();
    private Map<EMsgType, IMsgHandler> handlers = new HashMap<>();

    public int size() {
        return msgs.size();
    }

    protected T blockingTake() throws InterruptedException {
        return msgs.take();
    }
    public void blockingPut(T t) throws InterruptedException {
        msgs.put(t);
    }

    protected T nonBlockingTake() {
        return msgs.poll();
    }
    public boolean nonBlockingPut(T t) {
        return msgs.offer(t);
    }

    protected void handle(T msg) {
        IMsgHandler h = handlers.getOrDefault(msg.type(), null);

        if (h != null)
            h.handle(msg);
    }

    public void register(IMsgHandler<? extends Msg> handler) {
        handlers.put(handler.type(), handler);
    }
}
