package MsgHandler;

import Msg.*;

public interface IMsgHandler<T extends Msg> {

    void handle(T msg);
    EMsgType type();
}
