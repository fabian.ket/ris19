package InputSystem;

public abstract class CommandHandler {

    public abstract void handle();
}
