package InputSystem;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;

public class MovePlayerDownCommand extends MovePlayerCommand {

    public static final int KEY01 = GLFW_KEY_S;

    @Override
    public Vector3f execute() {
        return new Vector3f(0, -1, 0);
    }
}
