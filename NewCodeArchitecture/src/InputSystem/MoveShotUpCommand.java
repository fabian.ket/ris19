package InputSystem;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_I;

public class MoveShotUpCommand extends MoveShotCommand {

    public static final int KEY01 = GLFW_KEY_I;

    @Override
    public Vector3f execute() {
        return new Vector3f(0, 1, 0);
    }
}
