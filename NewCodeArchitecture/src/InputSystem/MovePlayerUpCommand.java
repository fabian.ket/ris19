package InputSystem;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;

public class MovePlayerUpCommand extends MovePlayerCommand {

    public static final int KEY01 = GLFW_KEY_W;

    @Override
    public Vector3f execute() {
        return new Vector3f(0, 1, 0);
    }
}
