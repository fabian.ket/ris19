package InputSystem;

public abstract class Command {

    public abstract ECommandType type();
}
