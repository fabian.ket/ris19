package InputSystem;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_J;

public class MoveShotLeftCommand extends MoveShotCommand {

    public static final int KEY01 = GLFW_KEY_J;

    @Override
    public Vector3f execute() {
        return new Vector3f(-1, 0, 0);
    }
}
