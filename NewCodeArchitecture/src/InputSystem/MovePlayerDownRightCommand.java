package InputSystem;

import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;

public class MovePlayerDownRightCommand extends MovePlayerCommand {

    public static final int KEY01 = GLFW_KEY_S;
    public static final int KEY02 = GLFW_KEY_D;

    @Override
    public Vector3f execute() { return new Vector3f(1, -1, 0); }
}
