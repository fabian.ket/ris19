package InputSystem;

import org.joml.Vector3f;

public abstract class MovePlayerCommand extends Command {

    public abstract Vector3f execute();

    @Override
    public ECommandType type() { return ECommandType.PLAYER_MOVE; }
}
