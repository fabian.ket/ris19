package InputSystem;

public abstract class ActionCommand extends Command {

    public abstract void execute();

    @Override
    public ECommandType type() { return ECommandType.ACTION; }
}
