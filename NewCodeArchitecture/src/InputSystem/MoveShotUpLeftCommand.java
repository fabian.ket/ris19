package InputSystem;

import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;

public class MoveShotUpLeftCommand extends MoveShotCommand {

    public static final int KEY01 = GLFW_KEY_I;
    public static final int KEY02 = GLFW_KEY_J;

    @Override
    public Vector3f execute() { return new Vector3f(-1, 1, 0); }
}
