package InputSystem;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;

public class Input {

    private static class InstanceHolder {
        private static final Input INSTANCE = new Input();
    }

    private Input() {}

    public static Input getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public List<Command> checkInput(long windowId) {
        List<Command> cmds = new ArrayList<>();

        // TODO: direct GLFW calls


        // PLAYER MOVES
        if (glfwGetKey(windowId, MovePlayerUpRightCommand.KEY01) == GLFW_TRUE &&
                glfwGetKey(windowId, MovePlayerUpRightCommand.KEY02) == GLFW_TRUE) {
            cmds.add(new MovePlayerUpRightCommand());
        }
        else if (glfwGetKey(windowId, MovePlayerUpLeftCommand.KEY01) == GLFW_TRUE &&
                glfwGetKey(windowId, MovePlayerUpLeftCommand.KEY02) == GLFW_TRUE) {
            cmds.add(new MovePlayerUpLeftCommand());
        }
        else if (glfwGetKey(windowId, MovePlayerDownRightCommand.KEY01) == GLFW_TRUE &&
                glfwGetKey(windowId, MovePlayerDownRightCommand.KEY02) == GLFW_TRUE) {
            cmds.add(new MovePlayerDownRightCommand());
        }
        else if (glfwGetKey(windowId, MovePlayerDownLeftCommand.KEY01) == GLFW_TRUE &&
                glfwGetKey(windowId, MovePlayerDownLeftCommand.KEY02) == GLFW_TRUE) {
            cmds.add(new MovePlayerDownLeftCommand());
        }

        else if (glfwGetKey(windowId, MovePlayerUpCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new MovePlayerUpCommand());
        }
        else if (glfwGetKey(windowId, MovePlayerDownCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new MovePlayerDownCommand());
        }

        else if (glfwGetKey(windowId, MovePlayerRightCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new MovePlayerRightCommand());
        }
        else if (glfwGetKey(windowId, MovePlayerLeftCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new MovePlayerLeftCommand());
        }


        // SHOT DIRECTION
        if (glfwGetKey(windowId, MoveShotUpRightCommand.KEY01) == GLFW_TRUE &&
                glfwGetKey(windowId, MoveShotUpRightCommand.KEY02) == GLFW_TRUE) {
            cmds.add(new MoveShotUpRightCommand());
        }
        else if (glfwGetKey(windowId, MoveShotUpLeftCommand.KEY01) == GLFW_TRUE &&
                glfwGetKey(windowId, MoveShotUpLeftCommand.KEY02) == GLFW_TRUE) {
            cmds.add(new MoveShotUpLeftCommand());
        }
        else if (glfwGetKey(windowId, MoveShotDownRightCommand.KEY01) == GLFW_TRUE &&
                glfwGetKey(windowId, MoveShotDownRightCommand.KEY02) == GLFW_TRUE) {
            cmds.add(new MoveShotDownRightCommand());
        }
        else if (glfwGetKey(windowId, MoveShotDownLeftCommand.KEY01) == GLFW_TRUE &&
                glfwGetKey(windowId, MoveShotDownLeftCommand.KEY02) == GLFW_TRUE) {
            cmds.add(new MoveShotDownLeftCommand());
        }

        else if (glfwGetKey(windowId, MoveShotUpCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new MoveShotUpCommand());
        }
        else if (glfwGetKey(windowId, MoveShotDownCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new MoveShotDownCommand());
        }

        else if (glfwGetKey(windowId, MoveShotRightCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new MoveShotRightCommand());
        }
        else if (glfwGetKey(windowId, MoveShotLeftCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new MoveShotLeftCommand());
        }


        // SHOOTING
        if (glfwGetKey(windowId, SpaceActionCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new SpaceActionCommand());
        }


        // QUIT GAME
        if (glfwGetKey(windowId, EscControlCommand.KEY01) == GLFW_TRUE) {
            cmds.add(new EscControlCommand());
        }

        return cmds;
    }
}
