package InputSystem;

import Game.Game;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;

public class EscControlCommand extends ControlCommand {

    public static final int KEY01 = GLFW_KEY_ESCAPE;

    @Override
    public void execute() { Game.getInstance().setRun(false); }
}
