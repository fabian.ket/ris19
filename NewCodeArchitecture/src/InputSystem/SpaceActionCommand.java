package InputSystem;

import Game.Game;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE;

public class SpaceActionCommand extends ActionCommand {

    public static final int KEY01 = GLFW_KEY_SPACE;

    @Override
    public void execute() { Game.getInstance().playerHasShoot = true; }
}
