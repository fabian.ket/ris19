package InputSystem;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;

public class MovePlayerLeftCommand extends MovePlayerCommand {

    public static final int KEY01 = GLFW_KEY_A;

    @Override
    public Vector3f execute() {
        return new Vector3f(-1, 0, 0);
    }
}
