package InputSystem;

import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;

public class MoveShotDownRightCommand extends MoveShotCommand {

    public static final int KEY01 = GLFW_KEY_K;
    public static final int KEY02 = GLFW_KEY_L;

    @Override
    public Vector3f execute() { return new Vector3f(1, -1, 0); }
}
