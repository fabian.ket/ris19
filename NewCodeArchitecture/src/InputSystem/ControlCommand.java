package InputSystem;

public abstract class ControlCommand extends Command {

    public abstract void execute();

    @Override
    public ECommandType type() { return ECommandType.CONTROL; }
}
