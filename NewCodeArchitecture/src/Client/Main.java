package Client;

import ArgsParser.*;
import Game.Game;

public class Main {

    public static void main(String[] args) {

        Arguments arguments = ArgsParser.parse(args);

        Game game = Game.getInstance();
        game.init(arguments.host, arguments.port, arguments.width, arguments.height);
        game.loop();
        game.exit();
    }
}
