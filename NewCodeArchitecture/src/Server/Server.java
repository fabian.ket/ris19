package Server;

import DataStructures.Quadtree;
import Game.*;
import Msg.*;
import MsgHandler.Handler;
import MsgHandler.IMsgHandler;
import NetworkSystem.ENetManagerType;
import NetworkSystem.NetManager;
import Utils.Math;
import Utils.Time;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Server extends Handler<Msg> {

    private static class InstanceHolder {
        private static final Server INSTANCE = new Server();
    }

    private static final Vector3f V3F_ZERO = new Vector3f();

    private enum EDirection {
        UP, DOWN, LEFT, RIGHT, DIAGONAL_UP_RIGHT, DIAGONAL_UP_LEFT, DIAGONAL_DOWN_RIGHT, DIAGONAL_DOWN_LEFT
    }


    private static float deltaTime = 0;

    private static final float EVENT_HZ = 1.f / 1.f;
    private static float eventUnprocessed = 0.f;

    private static final float ENEMY_MOVE_HZ = 1.f / 30.f;
    private static float enemyMoveUnprocessed = 0.f;

    private static final float SHOT_MOVE_HZ = 1.f / 30.f;
    private static float shotMoveUnprocessed = 0.f;

    private Random rand = new Random(System.currentTimeMillis());
    private Map<Integer, PlayerObject> players = new HashMap<>();
    private Map<Integer, EnemyObject> enemies  = new HashMap<>();
    private Map<Integer, ShotObject> shots   = new HashMap<>();

    private List<Collider> wallColliders;
    private Quadtree<Collider> colliderQuadtree = new Quadtree<>(
            -400, 225,
            Game.WIDTH, Game.HEIGHT,
            Game.WINDOW_WIDTH, Game.WINDOW_HEIGHT);

    private int playerNumberPool = 0;
    private int enemyNumberPool  = 10000;
    private int shotNumberPool   = 10000000;

    private static final int MAX_ENEMIES = 20;
    private static final float ENEMY_MOVE_SPEED = 100f;
    private static final float SHOT_MOVE_SPEED  = 500f;

    private static final long MAP_SEED = System.currentTimeMillis();

    private Server() {}

    public static Server getInstance() { return InstanceHolder.INSTANCE; }


    public void init(String host, int port, int width, int height) {

        Game.WIDTH  = width;
        Game.HEIGHT = height;

        register(new IMsgHandler<LoginMsg>() {
            @Override
            public void handle(LoginMsg msg) {

                PlayerObject p = new PlayerObject(playerNumberPool++);
                players.put(p.getId(), p);

                // TODO: hardcoded player start point
                // Matrix4f m = new Matrix4f().translate(Game.WINDOW_WIDTH / 2 - 20, -Game.WINDOW_HEIGHT / 2 + 20, 0);
                Matrix4f m = new Matrix4f().translate((-400) + Game.WIDTH / 2.f - 20, 225 + (-Game.HEIGHT) / 2.f + 20, 0);
                p.setModelMat(m);

                InitGameMsg pnm = new InitGameMsg(p.getId(), MAP_SEED);

                try {
                    msg.clientHandler.blockingPut(pnm);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }

                for (PlayerObject po : players.values()) {
                    PlayerMsg pm = new PlayerMsg(po.getId(), po.getModelMat(), po.hitByEnemy);

                    try {
                        NetManager.getInstance().blockingPut(pm);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

            }

            @Override
            public EMsgType type() { return EMsgType.LOGIN; }
        });

        register(new IMsgHandler<SendPlayerMsg>() {
            @Override
            public void handle(SendPlayerMsg msg) {



                PlayerObject p = players.getOrDefault(msg.playerNumber, null);

                if (p != null) {

                    Collider playerCollider = p.getCollider();

                    Matrix4f m = p.getModelMat();

                    if (!msg.direction.equals(V3F_ZERO)) {
                        Vector3f d = new Vector3f(msg.direction);
                        d.mul(Game.PLAYER_MOVE_SPEED);
                        m.translate(d);
                    }

                    playerCollider.mul(m);

                    p.hitByEnemy = false;

                    for (EnemyObject e : enemies.values()) {
                        Collider enemyCollider = e.getCollider();
                        enemyCollider.mul(e.getModelMat());

                        if (playerCollider.collision(enemyCollider)) {
                            p.hitByEnemy = true;
                            break;
                        }
                    }

                    List<Collider> colliders = colliderQuadtree.getElements(playerCollider.getCenter());

                    for(Collider c : colliders) {
                        if (playerCollider.collision(c)) {
                            m = p.getModelMat();
                            break;
                        }
                    }

                    try {
                        p.setModelMat(m);
                        p.setDirection(msg.direction);

                        NetManager.getInstance().blockingPut(new PlayerMsg(p.getId(), m, p.hitByEnemy));
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            }

            @Override
            public EMsgType type() { return EMsgType.SEND_PLAYER; }
        });

        register(new IMsgHandler<PlayerShotMsg>() {
            @Override
            public void handle(PlayerShotMsg msg) {
                int id = msg.playerNumber;

                PlayerObject p = players.getOrDefault(id, null);

                if (p != null) {

                    Vector3f d = msg.direction;

                    if (d.x > 0)
                        d.x = 1.f;
                    else if (d.x < 0)
                        d.x = -1.f;

                    if (d.y > 0)
                        d.y = 1.f;
                    else if (d.y < 0)
                        d.y = -1.f;

                    if (d.x == 0 && d.y == 0) {
                        d.x = 1;
                    }

                    Vector3f yAchses = new Vector3f(0,1,0);
                    float rotationAngle = d.angle(yAchses);

                    if (d.x > 0 && d.y > 0)
                        rotationAngle += java.lang.Math.PI + Math.HALF_PI;
                    else if (d.x > 0 && d.y < 0)
                        rotationAngle += Math.HALF_PI;
                    else if (d.x > 0)
                        rotationAngle += java.lang.Math.PI;

                    ShotObject shot = new ShotObject(shotNumberPool++, p.getModelMat(), d, rotationAngle);
                    shots.put(shot.getId(), shot);
                }
            }

            @Override
            public EMsgType type() { return EMsgType.PLAYER_SHOT; }
        });

        register(new IMsgHandler<SendLogoutMsg>() {
            @Override
            public void handle(SendLogoutMsg msg) {
                players.remove(msg.playerNumber);

                LogoutMsg lom = new LogoutMsg(msg.playerNumber);

                try {
                    NetManager.getInstance().blockingPut(lom);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public EMsgType type() { return EMsgType.SEND_LOGOUT; }
        });


        MapGenerator mapGenerator = new MapGenerator();
        mapGenerator.generateMap(Game.WIDTH, Game.HEIGHT, MAP_SEED, Game.RANDOM_FILL_PERCENT);
        wallColliders = mapGenerator.createColliderForMap(Game.WIDTH, Game.HEIGHT);

        colliderQuadtree.createQuadtree(wallColliders);


        NetManager netManager = NetManager.getInstance();
        netManager.verbose = 0;
        netManager.init(ENetManagerType.SERVER, host, port);

        Thread consoleThread = new Thread(this::inputHandling);
        consoleThread.start();
    }

    public void loop() {
        float t0;
        while(Game.getInstance().getRun()) {
            t0 = Time.getTime();

            {
                processMsgs();

                createNewEvents();

                moveShots();

                sendShots();

                moveEnemies();

                sendEnemies();
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

            deltaTime = Time.getTime() - t0;
        }
    }

    public void exit() { }

    private void processMsgs() {

        int size = size();

        for (int i = 0; i < size; i++) {
            try {
                Msg msg = blockingTake();
                handle(msg);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    private void createNewEvents() {
        eventUnprocessed += deltaTime;
        enemyMoveUnprocessed += deltaTime;

        if (eventUnprocessed >= EVENT_HZ) {

            try {
                createNewBackgroundColorEvent();
                createNewEnemyEvent();

            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

            eventUnprocessed = 0.f;
        }
    }

    private void createNewBackgroundColorEvent() throws InterruptedException {
        NetManager.getInstance().blockingPut(new EventBackgroundColorMsg());
    }

    private void createNewEnemyEvent() throws InterruptedException {
        if (enemies.size() < MAX_ENEMIES) {

            int id = enemyNumberPool++;
            Matrix4f mat = new Matrix4f();

            int boundX = Game.WIDTH  - Game.WINDOW_WIDTH;
            int boundY = Game.HEIGHT - Game.WINDOW_HEIGHT;

            if (boundX <= 0) boundX = 1;
            if (boundY <= 0) boundY = 1;

            float x =  rand.nextInt(boundX);
            float y = -rand.nextInt(boundY);

            mat.translate(x, y, 0);

            Vector3f v = new Vector3f(x, y, 0);
            Collider c = new Collider(v, EnemyObject.COLLIDER_RADIUS * 2);

            List<Collider> wallObstacles = colliderQuadtree.getElements(v);

            for (Collider wo : wallObstacles) {
                if (c.collision(wo)) {
                    return;
                }
            }

            EnemyObject enemy = new EnemyObject(id);
            enemy.setModelMat(mat);

            enemies.put(enemy.getId(), enemy);

            NetManager.getInstance().blockingPut(new EnemyMsg(id, mat));
        }
    }

    private EDirection getRandomEDirection() {
        EDirection result;

        int i = rand.nextInt(8);

        if (i == EDirection.UP.ordinal()) {
            result = EDirection.UP;
        }
        else if (i == EDirection.DOWN.ordinal()) {
            result = EDirection.DOWN;
        }
        else if (i == EDirection.LEFT.ordinal()) {
            result = EDirection.LEFT;
        }
        else if (i == EDirection.RIGHT.ordinal()) {
            result = EDirection.RIGHT;
        }
        else if (i == EDirection.DIAGONAL_UP_RIGHT.ordinal()) {
            result = EDirection.DIAGONAL_UP_RIGHT;
        }
        else if (i == EDirection.DIAGONAL_UP_LEFT.ordinal()) {
            result = EDirection.DIAGONAL_UP_LEFT;
        }
        else if (i == EDirection.DIAGONAL_DOWN_RIGHT.ordinal()) {
            result = EDirection.DIAGONAL_DOWN_RIGHT;
        }
        else {
            result = EDirection.DIAGONAL_DOWN_LEFT;
        }

        return result;
    }

    private Vector3f eDirectionToVector3f(EDirection eDirection) {
        Vector3f result;

        switch (eDirection) {
            case UP: {
                result = new Vector3f(0, 1, 0);
            } break;
            case DOWN: {
                result = new Vector3f(0, -1, 0);
            } break;
            case RIGHT: {
                result = new Vector3f(1, 0, 0);
            } break;
            case LEFT: {
                result = new Vector3f(-1, 0, 0);
            } break;
            case DIAGONAL_UP_RIGHT: {
                result = new Vector3f(1, 1, 0);
            } break;
            case DIAGONAL_UP_LEFT: {
                result = new Vector3f(-1, 1, 0);
            } break;
            case DIAGONAL_DOWN_RIGHT: {
                result = new Vector3f(1, -1, 0);
            } break;
            default: {
                result = new Vector3f(-1, -1, 0);
            } break;
        }

        return result;
    }

    private void moveEnemies() {
        for (EnemyObject enemy : enemies.values()) {

            Collider nearestObstacle = enemy.getNearestObstacle(colliderQuadtree.getElements(enemy.getCurrentPositionInSpace()));

            Vector3f vDirection;

            if (nearestObstacle != null &&
                    nearestObstacle.getCenter().distance(enemy.getCurrentPositionInSpace()) <=
                    (nearestObstacle.getRadius() + enemy.getCollider().getRadius() + 5)) {

                Vector3f enemyPos = enemy.getCurrentPositionInSpace();
                Vector3f obstaclePos = nearestObstacle.getCurrentPositionInSpace();

                vDirection = new Vector3f((enemyPos.x - obstaclePos.x),
                                          (enemyPos.y - obstaclePos.y),
                                          0);

                vDirection.normalize();

//                if (vDirection.x > 0) vDirection.x =  1.f;
//                if (vDirection.x < 0) vDirection.x = -1.f;
//
//                if (vDirection.y > 0) vDirection.y =  1.f;
//                if (vDirection.y < 0) vDirection.y = -1.f;

                Vector3f nearestPlayer = enemy.getDirectionToNearestPlayer(players);

                nearestPlayer.normalize();

//                if (nearestPlayer.x > 0) nearestPlayer.x =  1.f;
//                if (nearestPlayer.x < 0) nearestPlayer.x = -1.f;
//
//                if (nearestPlayer.y > 0) nearestPlayer.y =  1.f;
//                if (nearestPlayer.y < 0) nearestPlayer.y = -1.f;

                vDirection.x += nearestPlayer.x;
                vDirection.y += nearestPlayer.y;

            } else {
                vDirection = enemy.getDirectionToNearestPlayer(players);

                if (vDirection.x > 0) vDirection.x =  1;
                if (vDirection.x < 0) vDirection.x = -1;

                if (vDirection.y > 0) vDirection.y =  1;
                if (vDirection.y < 0) vDirection.y = -1;
            }

            float xRand = Math.fmap(rand.nextFloat(), -1.0f, 1.0f);
            float yRand = Math.fmap(rand.nextFloat(), -1.0f, 1.0f);

            vDirection.x += xRand;
            vDirection.y += yRand;

            // TODO: multiply move speed
            vDirection.mul(ENEMY_MOVE_SPEED * deltaTime);

            Matrix4f m = new Matrix4f(enemy.getModelMat());
            m.translate(vDirection);

            Collider enemyCollider = enemy.getCollider();
            enemyCollider.mul(m);

            boolean collision = false;
            for (PlayerObject p : players.values()) {
                Collider playerCollider = p.getCollider();
                playerCollider.mul(p.getModelMat());

                if (enemyCollider.collision(playerCollider)) {
                    collision = true;
                    p.hitByEnemy = true;

                    try {
                        NetManager.getInstance().blockingPut(new PlayerMsg(p.getId(), p.getModelMat(), p.hitByEnemy));
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }

                    break;
                }
            }

            if (!collision) {
                List<Collider> colliders = colliderQuadtree.getElements(enemyCollider.getCenter());
                for (Collider c : colliders) {
                    if (enemyCollider.collision(c)) {
                        collision = true;
                        break;
                    }
                }
            }

            if (!collision)
                enemy.setModelMat(m);
        }
    }

    private void sendEnemies() {

        enemyMoveUnprocessed += deltaTime;

        if (enemyMoveUnprocessed >= ENEMY_MOVE_HZ) {

            for (EnemyObject enemy : enemies.values()) {
                try {
                    NetManager.getInstance().blockingPut(new EnemyMsg(enemy.getId(), enemy.getModelMat()));
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            enemyMoveUnprocessed = 0.f;
        }
    }

    private void moveShots() {

        List<Integer> shotsToRemove = new ArrayList<>();

        for (ShotObject s : shots.values()) {

            Vector3f d = s.getDirection();
            Matrix4f m = s.getModelMat();

            d.mul(SHOT_MOVE_SPEED * deltaTime);

            m.translate(d);

            Collider shotCollider = s.getCollider();
            shotCollider.mul(m);

            Collider enemieCollider;

            for(EnemyObject e : enemies.values()) {
                enemieCollider = e.getCollider();
                enemieCollider.mul(e.getModelMat());

                if (shotCollider.collision(enemieCollider)) {
                    s.hit = true;

                    // remove objects
                    shotsToRemove.add(s.getId());
                    enemies.remove(e.getId());

                    RemoveShotMsg rsm = new RemoveShotMsg(s.getId());
                    EnemyKilledMsg ekm = new EnemyKilledMsg(e.getId());

                    try {
                        NetManager.getInstance().blockingPut(rsm);
                        NetManager.getInstance().blockingPut(ekm);
                    }
                    catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }

                    break;
                }
            }

            if (!s.hit) {

                List<Collider> colliders = colliderQuadtree.getElements(shotCollider.getCenter());

                for (Collider c : colliders) {
                    if (shotCollider.collision(c)) {

                        s.wallHitCounter--;

                        if (s.wallHitCounter < 0) {
                            s.hit = true;

                            shotsToRemove.add(s.getId());
                            RemoveShotMsg rsm = new RemoveShotMsg(s.getId());

                            try {
                                NetManager.getInstance().blockingPut(rsm);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        } else {
                            m = s.getModelMat();

                            Vector3f cPos = c.getCurrentPositionInSpace();
                            Vector3f sPos = s.getCurrentPositionInSpace();

                            Vector3f direction = new Vector3f(sPos.x - cPos.x, sPos.y - cPos.y, 0);
                            s.setDirection(direction.normalize());
                        }

                        break;
                    }
                }
            }

            if (s.wallHitCounter <= ShotObject.MAX_WALL_COLLISIONS - 1)
                s.rotaionAngle += 10 * 0.017453292519943f;

            if (!s.hit)
                s.setModelMat(m);
        }

        for (Integer id : shotsToRemove) {
            shots.remove(id);
        }
    }

    private void sendShots() {

        shotMoveUnprocessed += deltaTime;

        if (shotMoveUnprocessed >= SHOT_MOVE_HZ) {

            for (ShotObject s : shots.values()) {
                try {
                    NetManager.getInstance().blockingPut(new ShotMsg(s.getId(), s.getModelMat(), s.rotaionAngle));
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            shotMoveUnprocessed = 0.f;
        }
    }


    private void inputHandling() {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Quit Server with q or Q!");

        while (Game.getInstance().getRun()) {

            try {
                String input = reader.readLine();

                if (input.equals("q") || input.equals("Q")) {

                    try {
                        NetManager.getInstance().blockingPut(new StopMsg());
                        Game.getInstance().setRun(false);

                        return;

                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            } catch (IOException io) {
                io.printStackTrace();
            }
        }
    }
}
