package Server;

import ArgsParser.*;

public class Main {

    public static void main(String[] args) {
        Arguments arguments = ArgsParser.parse(args);

        Server server = Server.getInstance();
        server.init(arguments.host, arguments.port, arguments.width, arguments.height);
        server.loop();
        server.exit();
    }
}
