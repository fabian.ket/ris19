package Game;

import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// https://github.com/SebLague/Procedural-Cave-Generation

public class MapGenerator {
    private int width;
    private int height;

    // [Range(0,100)]
    private int randomFillPercent;

    private int[][] map;


    public ReturnArrays generateMap(int width, int height, long seed, int randomFillPercent) {

        // TODO: hardcoded stuff, but really good values
        this.width  = 80;
        this.height = 45;
        this.randomFillPercent = randomFillPercent;

        map = new int[this.width][this.height];
        randomFillMap(seed);

        for (int i = 0; i < Game.MAP_SMOOTHING_ROUNDS; i++) {
            smoothMap();
        }

        // printMap(map);

        MeshGenerator meshGen = new MeshGenerator();
        return meshGen.generateMesh(map, 1, width, height);
    }

    public List<Collider> createColliderForMap(int width, int height) {

        float radius = width / (2.f * map.length);

        float widthOffset  = width / 2.f  - Game.WINDOW_WIDTH / 2.f;
        float heightOffset = height / 2.f - Game.WINDOW_HEIGHT / 2.f;

        float smallestX = - width  / 2.f + radius + widthOffset;
        float smallestY = - height / 2.f + radius - heightOffset;

        List<Collider> result = new ArrayList<>();

        int[][] tmp = new int[map.length][map[0].length];

        for (int y = 0; y < map[0].length; y++) {
            for (int x = 0; x < map.length; x++) {


                if (x == 0 || x == map.length - 1 || y == 0 || y == map[0].length - 1) {
                    tmp[x][y] = 1;
                    continue;
                }
                else if (x == 1 || x == map.length - 2 || y == 1 || y == map[0].length - 2) {
                    tmp[x][y] = 1;
                    continue;
                }

                if (map[x][y] == 1) {

                    if (x + 1 < map.length) {
                        if (map[x + 1][y] == 0) {
                            tmp[x][y] = 1;
                            continue;
                        }
                    }

                    if (x - 1 > -1) {
                        if (map[x - 1][y] == 0) {
                            tmp[x][y] = 1;
                            continue;
                        }
                    }

                    if (y + 1 < map[0].length) {
                        if (map[x][y + 1] == 0) {
                            tmp[x][y] = 1;
                            continue;
                        }
                    }

                    if (y - 1 > -1) {
                        if (map[x][y - 1] == 0) {
                            tmp[x][y] = 1;
                            continue;
                        }
                    }
                }
            }
        }

        for (int y = 0; y < tmp[0].length; y++) {
            for (int x = 0; x < tmp.length; x++) {
                if (tmp[x][y] == 1) {
                    result.add(new Collider(new Vector3f(smallestX + (2.f*radius * x),smallestY + (2.f*radius * y),0), radius));
                }
            }
        }

        printMap(map);
        printMap(tmp);

        return result;
    }

    private void printMap(int[][] map) {
        for (int y = map[0].length - 1; y >= 0; y--) {
            for (int x = 0; x < map.length; x++) {
                if (map[x][y] == 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private void randomFillMap(long seed) {
        Random pseudoRandom = new Random(seed);

        for (int x = 0; x < width; x ++) {
            for (int y = 0; y < height; y ++) {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
                    map[x][y] = 1;
                }
                else if (x == 1 || x == width - 2 || y == 1 || y == height - 2) {
                    map[x][y] = 1;
                }
                else {
                    map[x][y] = (pseudoRandom.nextInt(101) < randomFillPercent) ? 1 : 0;
                }
            }
        }
    }

    private void smoothMap() {
        for (int x = 0; x < width; x ++) {
            for (int y = 0; y < height; y ++) {
                int neighbourWallTiles = getSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                    map[x][y] = 1;
				else if (neighbourWallTiles < 4)
                    map[x][y] = 0;
            }
        }
    }

    private int getSurroundingWallCount(int gridX, int gridY) {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX ++) {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY ++) {
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height) {
                    if (neighbourX != gridX || neighbourY != gridY) {
                        wallCount += map[neighbourX][neighbourY];
                    }
                }
                else {
                    wallCount ++;
                }
            }
        }
        return wallCount;
    }
}
