package Game;

import DataStructures.Quadtree;
import GraphicSystem.Model;
import GraphicSystem.Renderer;
import GraphicSystem.Texture;
import InputSystem.*;
import Msg.*;
import MsgHandler.Handler;
import MsgHandler.IMsgHandler;
import NetworkSystem.ENetManagerType;
import NetworkSystem.NetManager;
import Utils.Math;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.util.*;

// Singleton
public class Game extends Handler<Msg> {

    private static class InstanceHolder {
        private static final Game INSTANCE = new Game();
    }

    private PlayerObject player = new PlayerObject(-1);

    private Map<Integer, PlayerObject> players = new HashMap<>();
    private Map<Integer, EnemyObject>  enemies = new HashMap<>();
    private Map<Integer, ShotObject>   shots   = new HashMap<>();
    private Map<Integer, WallObject>   walls   = new HashMap<>();
    private Map<Integer, GameObject>   gameObjects = new HashMap<>();


    private Input input = Input.getInstance();
    private Renderer renderer = Renderer.getInstance();
    private NetManager netManager = NetManager.getInstance();

    private boolean run = true;

    private final Object INIT_LOCK = new Object();
    private byte initSystemCount = 0;
    private final byte MAX_INIT_SYSTEMS = 3;

    private final Object LOOP_LOCK = new Object();
    private byte loopSystemCount = 0;
    private final byte MAX_LOOP_SYSTEMS = 2;

    private final Object EXIT_LOCK = new Object();
    private byte exitSystemCount = 0;
    private final byte MAX_EXIT_SYSTEMS = 2;

    public static final float DIRECTION_SEND_RATE = 1.f / 60.f;
    private float directionUnprocessed = 0;

    public static final float SHOT_SEND_RATE = 1.f / 5.f;
    private float shotUnprocessed = 0;

    public boolean playerHasShoot = false;

    public static final float PLAYER_MOVE_SPEED = 250.f;

    public static int WIDTH  = 1600;
    public static int HEIGHT =  900;

    public static final int WINDOW_WIDTH  = 800;
    public static final int WINDOW_HEIGHT = 450;

    public static final int RANDOM_FILL_PERCENT = 35;
    public static final int MAP_SMOOTHING_ROUNDS = 50;

    private int staticNumberPool = 2000000000;

//    private Quadtree<WallObject> wallObjectQuadtree = new Quadtree<>(
//            -400, 225,
//            Game.WIDTH, Game.HEIGHT,
//            Game.WINDOW_WIDTH, Game.WINDOW_HEIGHT
//    );

    private Game() {}

    public static Game getInstance() {
        return InstanceHolder.INSTANCE;
    }


    public void init(String host, int port, int width, int height) {

        WIDTH  = width;
        HEIGHT = height;

        register(new IMsgHandler<ModelTextureCreatedMsg>() {
            @Override
            public void handle(ModelTextureCreatedMsg msg) {

                GameObject g = gameObjects.getOrDefault(msg.id, null);
                if (g != null) {
                    g.init(msg.m, msg.t1, msg.t2);
                }
            }

            @Override
            public EMsgType type() {
                return EMsgType.MODEL_TEXTURE_CREATED;
            }
        });

        register(new IMsgHandler<InitGameMsg>() {
            @Override
            public void handle(InitGameMsg msg) {
                player.id = msg.playerNumber;

                players.put(player.id, player);

                // TODO: hardcoded player start point
                Matrix4f m = new Matrix4f().translate((-400 + Game.WIDTH) / 2 - 20, (225 + (-Game.HEIGHT)) / 2 + 20, 0);
                player.setModelMat(m);

                gameObjects.put(player.id, player);

                CreateModelTextureMsg cmtmPlayer = new CreateModelTextureMsg(player.id,
                        Model.VERTICES, Model.INDICES, Model.TEXTURE_COORDINATES,
                        Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.GREEN,
                        Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.WHITE,
                        m);



                MapGenerator mapGenerator = new MapGenerator();
                ReturnArrays arrays = mapGenerator.generateMap(WIDTH, HEIGHT, msg.seed, RANDOM_FILL_PERCENT);


                // TODO: not in use, renderer starts to hang
//                int[] indices = new int[] {0, 1, 2};
//
//                for (int i = 0; i < arrays.indices.length; i+=3) {
//                    float x0 = arrays.vertices[arrays.indices[i] * 3];
//                    float y0 = arrays.vertices[arrays.indices[i] * 3 + 1];
//                    float z0 = arrays.vertices[arrays.indices[i] * 3 + 2];
//
//                    float x1 = arrays.vertices[arrays.indices[i + 1] * 3];
//                    float y1 = arrays.vertices[arrays.indices[i + 1] * 3 + 1];
//                    float z1 = arrays.vertices[arrays.indices[i + 1] * 3 + 2];
//
//                    float x2 = arrays.vertices[arrays.indices[i + 2] * 3];
//                    float y2 = arrays.vertices[arrays.indices[i + 2] * 3 + 1];
//                    float z2 = arrays.vertices[arrays.indices[i + 2] * 3 + 2];
//
//                    float[] vertices = new float[] {
//                            x0, y0, z0,
//                            x1, y1, z1,
//                            x2, y2, z2,
//                    };
//
//                    float[] texCoords = new float[] {
//                            0, 0,
//                            0, 1,
//                            1, 1
//                    };
//
//                    int id = staticNumberPool++;
//                    WallObject w = new WallObject(id);
//
//                    w.setCenter(new Vector3f((x0 + x1 + x2) / 3.f, (y0 + y1 + y2) / 3.f, (y0 + y1 + y2) / 3.f));
//
//                    walls.put(w.id, w);
//                    gameObjects.put(w.id, w);
//
//                    CreateModelTextureMsg cmtmWall = new CreateModelTextureMsg(
//                            w.id, vertices, indices, texCoords,
//                            Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.WHITE,
//                            Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.WHITE,
//
//                            // TODO: hardcoded wall transformation matrix
//                            new Matrix4f());
//
//                    try {
//                        renderer.blockingPut(cmtmWall);
//                    } catch (InterruptedException ie) {
//                        ie.printStackTrace();
//                    }
//                }

//                wallObjectQuadtree.createQuadtree(new ArrayList<>(walls.values()));

                int id = staticNumberPool++;
                WallObject w = new WallObject(id);

                walls.put(w.id, w);
                gameObjects.put(w.id, w);

                CreateModelTextureMsg cmtmWall = new CreateModelTextureMsg(
                        w.id, arrays.vertices, arrays.indices, arrays.texCoords,
                        Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.GRAY,
                        Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.GRAY,

                        // TODO: hardcoded wall transformation matrix
                        new Matrix4f());

                try {
                    renderer.blockingPut(cmtmPlayer);
                    renderer.blockingPut(cmtmWall);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public EMsgType type() { return EMsgType.INIT_GAME; }
        });

        register(new IMsgHandler<PlayerMsg>() {
            @Override
            public void handle(PlayerMsg msg) {

                if (msg.playerNumber != -1) {
                    PlayerObject p = players.getOrDefault(msg.playerNumber, null);

                    if (p == null) {
                        p = new PlayerObject(msg.playerNumber);
                        p.hitByEnemy = msg.hitByEnemy;

                        players.put(p.id, p);
                        gameObjects.put(p.id, p);

                        CreateModelTextureMsg cmtm = new CreateModelTextureMsg(
                            msg.playerNumber,
                            Model.VERTICES, Model.INDICES, Model.TEXTURE_COORDINATES,
                            Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.BLUE,
                            Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.WHITE,
                            msg.modelMat
                        );

                        try {
                            renderer.blockingPut(cmtm);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                    else {
                        if (msg.modelMat != null) {
                            p.hitByEnemy = msg.hitByEnemy;
                            p.setModelMat(msg.modelMat);
                        }
                    }
                }
            }

            @Override
            public EMsgType type() { return EMsgType.PLAYER; }
        });

        register(new IMsgHandler<EnemyMsg>() {
            @Override
            public void handle(EnemyMsg msg) {

                if (msg.id != -1) {
                    EnemyObject e = enemies.getOrDefault(msg.id, null);

                    if (e == null) {
                        e = new EnemyObject(msg.id);

                        enemies.put(e.id, e);
                        gameObjects.put(e.id, e);

                        CreateModelTextureMsg cmtm = new CreateModelTextureMsg(
                                e.getId(),
                                Model.CIRCLE, Model.CIRCLE_INDICES, Model.CIRCLE_TEXTURE,
                                Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.RED,
                                Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.WHITE,
                                msg.modelMat
                        );

                        try {
                            renderer.blockingPut(cmtm);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                    else {
                        if (msg.modelMat != null)
                            e.setModelMat(msg.modelMat);
                    }
                }
            }

            @Override
            public EMsgType type() { return EMsgType.ENEMY; }
        });

        register(new IMsgHandler<ShotMsg>() {
            @Override
            public void handle(ShotMsg msg) {

                if (msg.id != -1) {
                    ShotObject s = shots.getOrDefault(msg.id, null);

                    if (s == null) {
                        s = new ShotObject(msg.id);

                        shots.put(s.getId(), s);
                        gameObjects.put(s.getId(), s);

                        CreateModelTextureMsg cmtm = new CreateModelTextureMsg(
                                s.getId(),
                                Model.TRIANGLE, Model.TRIANGLE_INDICES, Model.TRIANGLE_TEXTURE,
                                Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.YELLOW,
                                Texture.TEXTURE_WIDTH, Texture.TEXTURE_HEIGHT, Texture.WHITE,
                                msg.modelMat.rotateZ(msg.rotationAngle)
                        );

                        try {
                            renderer.blockingPut(cmtm);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                    else {
                        if (msg.modelMat != null)
                            s.setModelMat(msg.modelMat.rotateZ(msg.rotationAngle));
                    }
                }
            }

            @Override
            public EMsgType type() { return EMsgType.SHOT; }
        });

        register(new IMsgHandler<RemoveShotMsg>() {
            @Override
            public void handle(RemoveShotMsg msg) {
                shots.remove(msg.id);
                gameObjects.remove(msg.id);
            }

            @Override
            public EMsgType type() { return EMsgType.REMOVE_SHOT; }
        });

        register(new IMsgHandler<EnemyKilledMsg>() {
            @Override
            public void handle(EnemyKilledMsg msg) {
                enemies.remove(msg.id);
                gameObjects.remove(msg.id);
            }

            @Override
            public EMsgType type() { return EMsgType.ENEMY_KILLED; }
        });

        register(new IMsgHandler<LogoutMsg>() {
            @Override
            public void handle(LogoutMsg msg) {
                players.remove(msg.playerNumber);
                gameObjects.remove(msg.playerNumber);
            }

            @Override
            public EMsgType type() { return EMsgType.LOGOUT; }
        });


        renderer.init(WINDOW_WIDTH, WINDOW_HEIGHT, "shader");

        netManager.verbose = 0;
        netManager.init(ENetManagerType.CLIENT, host, port);

        Thread chatThread = new Thread(this::chatHandling);
        chatThread.start();

        synchronizeInit();
    }

    public void loop() {

        LoginMsg login = new LoginMsg(-1, null);

        try {
            netManager.blockingPut(login);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

        while (run) {

            processMsgs();

            List<Command> cmds = input.checkInput(renderer.getWindowId());
            processInput(cmds);

            sendPlayerDirection();
            sendPlayerShot();

            checkCameraPosition();

            List<GameObject> toRender = prepareRenderObjects();
            renderer.setRenderObjects(toRender);

            synchronizeLoop();
        }
    }

    public void exit() {

        try {
            // netManager.blockingPut(new SendLogoutMsg(player.getId()));
            netManager.sendDirectly(new SendLogoutMsg(player.getId()));
            netManager.blockingPut(new StopMsg());
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

        synchronizeExit();
    }

    public void synchronizeInit() {
        synchronized (INIT_LOCK) {
            initSystemCount++;

            if (initSystemCount >= MAX_INIT_SYSTEMS) {
                initSystemCount = 0;
                INIT_LOCK.notifyAll();
            } else {
                try {
                    INIT_LOCK.wait();
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }

    public void synchronizeLoop() {
        synchronized (LOOP_LOCK) {
            loopSystemCount++;

            if (loopSystemCount >= MAX_LOOP_SYSTEMS) {
                loopSystemCount = 0;
                LOOP_LOCK.notifyAll();
            } else {
                try {
                    LOOP_LOCK.wait();
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }

    public void synchronizeExit() {
        synchronized (EXIT_LOCK) {
            exitSystemCount++;

            if (exitSystemCount >= MAX_EXIT_SYSTEMS) {
                exitSystemCount = 0;
                EXIT_LOCK.notifyAll();
            } else {
                try {
                    EXIT_LOCK.wait();
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }

    public boolean getRun() {return run; }
    public void setRun(boolean r) { run = r; }

    private void processMsgs() {

        int size = size();

        for (int i = 0; i < size; i++) {
            try {
                Msg msg = blockingTake();
                handle(msg);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    private void processInput(List<Command> cmds) {
        Vector3f v = new Vector3f();

        for (Command cmd : cmds) {

            if (cmd.type() == ECommandType.PLAYER_MOVE) {
                MovePlayerCommand mpc = (MovePlayerCommand) cmd;
                v.add(mpc.execute());
            }

            else if (cmd.type() == ECommandType.SHOT_MOVE) {
                MoveShotCommand msc = (MoveShotCommand) cmd;
                player.setShotDirection(msc.execute());
            }

            else if (cmd.type() == ECommandType.ACTION) {
                ActionCommand ac = (ActionCommand) cmd;
                ac.execute();
            }

            else if (cmd.type() == ECommandType.CONTROL) {
                ControlCommand cc = (ControlCommand) cmd;
                cc.execute();
            }
        }

        v.mul(renderer.getDeltaTime());

        Vector3f d = player.getDirection();
        d.add(v);

        player.setDirection(d);
    }

    private void sendPlayerDirection() {

        directionUnprocessed += renderer.getDeltaTime();

        if (directionUnprocessed >= DIRECTION_SEND_RATE) {

            SendPlayerMsg msg = new SendPlayerMsg(player.id, player.getDirection());

            try {
                netManager.blockingPut(msg);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

            directionUnprocessed = 0.f;

            player.setDirection(new Vector3f());
        }
    }

    private void sendPlayerShot() {
        shotUnprocessed += renderer.getDeltaTime();

        if (shotUnprocessed >= SHOT_SEND_RATE && playerHasShoot) {

            PlayerShotMsg msg = new PlayerShotMsg(player.getId(), player.getShotDirection());

            try {
                netManager.blockingPut(msg);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

            playerHasShoot = false;
            shotUnprocessed = 0.f;
        }
    }

    private void checkCameraPosition() {

        Vector3f c = player.getCenter();
        Matrix4f m = player.getModelMat();

        c = Math.mul(c, m);

        int x = (int) ((c.x + WINDOW_WIDTH / 2.f)  / (WINDOW_WIDTH));
        int y = (int) ((c.y - WINDOW_HEIGHT / 2.f) / (WINDOW_HEIGHT));

        c.x = -WINDOW_WIDTH  * x;
        c.y = -WINDOW_HEIGHT * y;
        c.z = 0;

        renderer.setCameraPosition(c);
    }

    private List<GameObject> prepareRenderObjects() {
        List<GameObject> toRender = new ArrayList<>();


//        List<WallObject> wallList = wallObjectQuadtree.getElements(player.getCurrentPositionInSpace());
        for (WallObject w : walls.values()) {
            if (w.renderable())
                toRender.add(w);
        }

        for (ShotObject s : shots.values()) {
            if (s.renderable())
                toRender.add(s);
        }

        for (EnemyObject e : enemies.values()) {
            if (e.renderable())
                toRender.add(e);
        }

        for (PlayerObject p : players.values()) {
            if (p.renderable())
                toRender.add(p);
        }

        return toRender;
    }

    private void chatHandling() {

        Scanner chatScanner = new Scanner(System.in);
        String input;

        while (Game.getInstance().getRun()) {
            try {
                if (System.in.available() > 0) {
                    input = chatScanner.nextLine();

                    try {
                        NetManager.getInstance().blockingPut(new SendChatMsg("Client " + player.getId() + " : " + input));
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                } else {
                    Thread.sleep(200);
                }
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }
    }
}
