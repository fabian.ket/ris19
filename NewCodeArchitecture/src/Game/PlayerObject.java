package Game;

import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;

public class PlayerObject extends GameObject {

    private Vector3f direction = new Vector3f();
    private Vector3f shotDirection = new Vector3f(1,0,0);

    public boolean hitByEnemy = false;
    private int flip = 0;

    public PlayerObject(int id) {
        super(id);
        drawMode = GL_LINE_LOOP;
    }


    public Vector3f getDirection() { return new Vector3f(direction); }
    public void setDirection(Vector3f v) { direction = new Vector3f(v); }

    public Vector3f getShotDirection() { return new Vector3f(shotDirection); }
    public void setShotDirection(Vector3f v) { shotDirection = new Vector3f(v); }

    @Override
    public boolean render() {

        if (tex1 == null || tex2 == null || model == null) {
            return false;
        }


        if (hitByEnemy) {
            flip = 1 - flip;

            if (flip == 1) {
                tex2.bind(0);
            }
            else {
                tex1.bind(0);
            }
        } else {
            tex1.bind(0);
        }

        model.render(drawMode);

        return true;
    }
}
