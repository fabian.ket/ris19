package Game;

import DataStructures.ILocatable;
import GraphicSystem.Model;
import GraphicSystem.Texture;
import Utils.Math;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.GL_TRIANGLES;

public abstract class GameObject implements ILocatable {

    public static final float COLLIDER_RADIUS = 10f;

    protected int id = -1;
    protected Model model = new Model();
    protected Texture tex1 = new Texture();
    protected Texture tex2 = new Texture();

    protected Collider collider = new Collider(Model.COLLIDER);

    protected int drawMode = GL_TRIANGLES;

    public GameObject(int id) {
        this.id = id;
    }

    public boolean init(float[] vertices, int[] indices, float[] tex_coordinates,
                        int tex_width, int tex_height, byte[] tex_data) {

        model = new Model();
        model.init(vertices, indices, tex_coordinates);

        tex1 = new Texture();
        tex1.init(tex_width, tex_height, tex_data);

        tex2 = new Texture();
        tex2.init(tex_width, tex_height, tex_data);

        return true;
    }

    public boolean init(Model m, Texture t1, Texture t2) {
        model = m;
        tex1 = t1;
        tex2 = t2;

        return true;
    }

    public boolean render() {

        if (tex1 == null || model == null) {
            return false;
        }

        tex1.bind(0);
        model.render(drawMode);

        return true;
    }

    public int getId() { return id; }

    public boolean renderable() { return model.getRenderable() && tex1.getRenderable(); }

    public Collider getCollider() { return new Collider(collider); }

    public Matrix4f getModelMat() {
        return (model != null) ? new Matrix4f(model.getModelMat()) : null;
    }

    public void setModelMat(Matrix4f mat) { if (model != null) model.setModelMat(mat); }

    public Vector3f getCenter() { return collider.getCenter(); }

    public Vector3f getCurrentCenterPosition() { return Math.mul(getCenter(), getModelMat()); }

    @Override
    public Vector3f getCurrentPositionInSpace() { return getCurrentCenterPosition(); }
}
