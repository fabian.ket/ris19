package Game;

import DataStructures.ILocatable;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class Collider implements ILocatable {

    private Vector3f center;
    private float radius;

    public Collider(Vector3f c, float r) {
        center = c;
        radius = r;
    }

    public Collider(Collider c) {
        center = new Vector3f(c.center);
        radius = c.radius;
    }

    public void mul(Matrix4f m) {
        Vector4f v = new Vector4f(center.x, center.y, center.z, 1);
        v.mul(m);

        center.x = v.x;
        center.y = v.y;

        // TODO: hardcoded vector z = 0
        center.z = 0.f;
    }

    public Vector3f getCenter() {
        return new Vector3f(center);
    }
    public void setCenter(Vector3f v) { center = new Vector3f(v); }

    public float getRadius() { return radius; }

    public boolean collision(Collider c) { return (this.center.distance(c.center)) <= (this.radius + c.radius); }

    @Override
    public Vector3f getCurrentPositionInSpace() { return new Vector3f(center); }
}
