package Game;

import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL32.GL_LINES_ADJACENCY;

public class WallObject extends GameObject {

    public WallObject(int id) {
        super(id);

        drawMode = GL_TRIANGLES;
    }

    public void setCenter(Vector3f v) { this.collider.setCenter(v); }
}
