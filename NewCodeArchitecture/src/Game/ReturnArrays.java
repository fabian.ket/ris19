package Game;

public class ReturnArrays {
    public float[] vertices;
    public int[] indices;
    public float[] texCoords;

    public ReturnArrays(float[] verts, int[] inds, float[] texs) {
        vertices = verts;
        indices = inds;
        texCoords = texs;
    }
}
