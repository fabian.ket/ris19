package Game;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;

public class ShotObject extends GameObject {

    public static final int MAX_WALL_COLLISIONS = 2;

    private Vector3f direction = new Vector3f();
    public boolean hit = false;
    public  float rotaionAngle = 0.f;
    public int wallHitCounter = MAX_WALL_COLLISIONS;

    public ShotObject(int id) {
        super(id);
        drawMode = GL_LINE_LOOP;
    }
    public ShotObject(int id, Matrix4f m, Vector3f v, float a) {
        super(id);
        setModelMat(m);
        direction = v;
        collider = new Collider(new Vector3f(), 6);

        rotaionAngle = a;
    }

    public Vector3f getDirection() { return new Vector3f(direction); }
    public void setDirection(Vector3f v) { direction = v; }
}
