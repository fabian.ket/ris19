package Game;

import AI.AI;
import org.joml.Vector3f;

import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;

public class EnemyObject extends GameObject {

    public static final float COLLIDER_RADIUS = 9f;

    private AI ai = new AI();

    public EnemyObject(int id) {
        super(id);
        collider = new Collider(new Vector3f(), COLLIDER_RADIUS);

        drawMode = GL_LINE_LOOP;
    }

    public Vector3f getDirectionToNearestPlayer(Map<Integer, PlayerObject> players) {
        return ai.getDirectionToNearestPlayer(this, players);
    }

    public Collider getNearestObstacle(List<Collider> obstacles) {
        return ai.getNearestObstacle(this, obstacles);
    }
}
