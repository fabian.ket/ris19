package Game;

import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


// https://github.com/SebLague/Procedural-Cave-Generation

public class MeshGenerator {
    private List<Vector3f> vertices;
    private List<Integer> indices;

    public ReturnArrays generateMesh(int[][] map, float squareSize, int width, int height) {
        SquareGrid squareGrid = new SquareGrid(map, squareSize);

        vertices = new ArrayList<>();
        indices = new ArrayList<>();

        for (int x = 0; x < squareGrid.squares.length; x ++) {
            for (int y = 0; y < squareGrid.squares[0].length; y ++) {
                triangulateSquare(squareGrid.squares[x][y]);
            }
        }


        float[] _vertices  = verticesToArray(vertices, map.length, map[0].length, width, height);
        int[] _indices   = indicesToArray(indices);
        float[] _texCoords = verticesToTexCoords(vertices);

        return new ReturnArrays(_vertices, _indices, _texCoords);
    }

    private float[] verticesToArray(List<Vector3f> vertices, int mapWidth, int mapHeight, float width, float height) {
        float[] result = new float[vertices.size() * 3];

        float halfMapWidth = (mapWidth / 2.f);
        float halfWidth = (width / 2.f);
        float halfMapHeight = (mapHeight / 2.f);
        float halfHeight = (height / 2.f);

        float widthOffset  = halfWidth  - Game.WINDOW_WIDTH / 2.f;
        float heightOffset = halfHeight - Game.WINDOW_HEIGHT / 2.f;

        int i = 0;
        for (Vector3f vertex : vertices) {
            result[i++] = ((vertex.x / halfMapWidth)  * halfWidth) + widthOffset;
            result[i++] = ((vertex.y / halfMapHeight) * halfHeight) - heightOffset;
            result[i++] = vertex.z;
        }

        return result;
    }

    private int[] indicesToArray(List<Integer> indices) {
        int[] result = new int[indices.size()];

        int i = 0;
        for (Integer index : indices) {
            result[i++] = index;
        }

        return result;
    }

    // TODO: yeah
    private float[] verticesToTexCoords(List<Vector3f> vertices) {
        float[] result = new float[vertices.size() * 2];

        Random rand = new Random();

        int j = 0;
        for (int i = 0;  i < vertices.size(); i++) {
            result[j++] = rand.nextInt(2);
            result[j++] = rand.nextInt(2);
        }

        return result;
    }

    private void triangulateSquare(Square square) {
        switch (square.configuration) {
            case 0:
                break;

            // 1 points:
            case 1:
                meshFromPoints(square.centreBottom, square.bottomLeft, square.centreLeft);
                break;
            case 2:
                meshFromPoints(square.centreRight, square.bottomRight, square.centreBottom);
                break;
            case 4:
                meshFromPoints(square.centreTop, square.topRight, square.centreRight);
                break;
            case 8:
                meshFromPoints(square.topLeft, square.centreTop, square.centreLeft);
                break;

            // 2 points:
            case 3:
                meshFromPoints(square.centreRight, square.bottomRight, square.bottomLeft, square.centreLeft);
                break;
            case 6:
                meshFromPoints(square.centreTop, square.topRight, square.bottomRight, square.centreBottom);
                break;
            case 9:
                meshFromPoints(square.topLeft, square.centreTop, square.centreBottom, square.bottomLeft);
                break;
            case 12:
                meshFromPoints(square.topLeft, square.topRight, square.centreRight, square.centreLeft);
                break;
            case 5:
                meshFromPoints(square.centreTop, square.topRight, square.centreRight, square.centreBottom, square.bottomLeft, square.centreLeft);
                break;
            case 10:
                meshFromPoints(square.topLeft, square.centreTop, square.centreRight, square.bottomRight, square.centreBottom, square.centreLeft);
                break;

            // 3 point:
            case 7:
                meshFromPoints(square.centreTop, square.topRight, square.bottomRight, square.bottomLeft, square.centreLeft);
                break;
            case 11:
                meshFromPoints(square.topLeft, square.centreTop, square.centreRight, square.bottomRight, square.bottomLeft);
                break;
            case 13:
                meshFromPoints(square.topLeft, square.topRight, square.centreRight, square.centreBottom, square.bottomLeft);
                break;
            case 14:
                meshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.centreBottom, square.centreLeft);
                break;

            // 4 point:
            case 15:
                meshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.bottomLeft);
                break;
        }

    }

    private void meshFromPoints(Node... points) {
        assignVertices(points);

        if (points.length >= 3)
            createTriangle(points[0], points[1], points[2]);
        if (points.length >= 4)
            createTriangle(points[0], points[2], points[3]);
        if (points.length >= 5)
            createTriangle(points[0], points[3], points[4]);
        if (points.length >= 6)
            createTriangle(points[0], points[4], points[5]);

    }

    private void assignVertices(Node[] points) {
        for (int i = 0; i < points.length; i ++) {
            if (points[i].vertexIndex == -1) {
                points[i].vertexIndex = vertices.size();
                vertices.add(points[i].position);
            }
        }
    }

    private void createTriangle(Node a, Node b, Node c) {
        indices.add(a.vertexIndex);
        indices.add(b.vertexIndex);
        indices.add(c.vertexIndex);
    }

    private class SquareGrid {
        private  Square[][] squares;

        private SquareGrid(int[][] map, float squareSize) {
            int nodeCountX = map.length;
            int nodeCountY = map[0].length;
            float mapWidth = nodeCountX * squareSize;
            float mapHeight = nodeCountY * squareSize;

            ControlNode[][] controlNodes = new ControlNode[nodeCountX][nodeCountY];

            for (int x = 0; x < nodeCountX; x ++) {
                for (int y = 0; y < nodeCountY; y ++) {
                    Vector3f pos = new Vector3f(-mapWidth/2 + x * squareSize + squareSize/2, -mapHeight/2 + y * squareSize + squareSize/2, 0);
                    controlNodes[x][y] = new ControlNode(pos, map[x][y] == 1, squareSize);
                }
            }

            squares = new Square[nodeCountX - 1][nodeCountY - 1];
            for (int x = 0; x < nodeCountX-1; x++) {
                for (int y = 0; y < nodeCountY-1; y++) {
                    squares[x][y] = new Square(controlNodes[x][y+1], controlNodes[x+1][y+1], controlNodes[x+1][y], controlNodes[x][y]);
                }
            }
        }
    }

    private class Square {

        private ControlNode topLeft, topRight, bottomRight, bottomLeft;
        private Node centreTop, centreRight, centreBottom, centreLeft;
        private int configuration;

        private Square (ControlNode _topLeft, ControlNode _topRight, ControlNode _bottomRight, ControlNode _bottomLeft) {
            topLeft = _topLeft;
            topRight = _topRight;
            bottomRight = _bottomRight;
            bottomLeft = _bottomLeft;

            centreTop = topLeft.right;
            centreRight = bottomRight.above;
            centreBottom = bottomLeft.right;
            centreLeft = bottomLeft.above;

            if (topLeft.active)
                configuration += 8;
            if (topRight.active)
                configuration += 4;
            if (bottomRight.active)
                configuration += 2;
            if (bottomLeft.active)
                configuration += 1;
        }

    }

    private class Node {
        protected Vector3f position;
        private int vertexIndex = -1;

        private Node(Vector3f _pos) {
            position = _pos;
        }
    }

    private class ControlNode extends Node {

        private boolean active;
        private Node above;
        private Node right;

        private ControlNode(Vector3f _pos, boolean _active, float squareSize) {
		    super(_pos);
            active = _active;

            // TODO: maybe forward vec is worng in Unity it is (0,0,1)
            Vector3f forward = new Vector3f(0,1,0);
            forward.mul(squareSize/2f);
            forward.add(position);

            Vector3f _right = new Vector3f(1,0,0);
            _right.mul(squareSize/2f);
            _right.add(position);

            above = new Node(forward);
            right = new Node(_right);
        }
    }
}