package DataStructures;

import org.joml.Vector3f;

public interface ILocatable {

    Vector3f getCurrentPositionInSpace();
}
