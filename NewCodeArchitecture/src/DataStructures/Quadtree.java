package DataStructures;

import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class Quadtree<T extends ILocatable> {

    private Quadtree<T> topLeft  = null;
    private Quadtree<T> topRight = null;
    private Quadtree<T> bottomLeft  = null;
    private Quadtree<T> bottomRight = null;

    private List<T> elements = null;

    private Vector3f center;

    public Quadtree(float gameStartX, float gameStartY,
                    int gameFieldWidth, int gameFieldHeight,
                    int windowWidth, int windowHeight) {

        float x = gameFieldWidth  / 2.f + gameStartX;
        float y = gameFieldHeight / 2.f + gameStartY;

        //TODO: hardcoded z
        center = new Vector3f(x, y, 0);

        if ( (gameFieldWidth / windowWidth) <= 1 ) {
            elements = new ArrayList<>();
        }
        else {
            int nextGameFieldWidth  = gameFieldWidth  >>> 1;
            int nextGameFieldHeight = gameFieldHeight >>> 1;

            topLeft = new Quadtree<>(gameStartX, gameStartY,
                    nextGameFieldWidth, nextGameFieldHeight,
                    windowWidth, windowHeight);
            topRight = new Quadtree<>(gameStartX + windowWidth, gameStartY,
                    nextGameFieldWidth, nextGameFieldHeight,
                    windowWidth, windowHeight);

            bottomLeft = new Quadtree<>(gameStartX, gameStartY - windowHeight,
                    nextGameFieldWidth, nextGameFieldHeight,
                    windowWidth, windowHeight);
            bottomRight = new Quadtree<>(gameStartX + windowWidth, gameStartY - windowHeight,
                    nextGameFieldWidth, nextGameFieldHeight,
                    windowWidth, windowHeight);
        }
    }

    private Quadtree<T> getElementsForPosition(Vector3f v) {

        if (elements != null) {
            return this;
        }

        if (v.x <= center.x && v.y >= center.y) {
            return topLeft.getElementsForPosition(v);
        }

        else if (v.x <= center.x && v.y < center.y) {
            return bottomLeft.getElementsForPosition(v);
        }

        else if (v.x > center.x && v.y >= center.y) {
            return topRight.getElementsForPosition(v);
        }

        else {
            return bottomRight.getElementsForPosition(v);
        }
    }

    public void createQuadtree(List<T> l) {

        for (T t : l) {
            Vector3f c = t.getCurrentPositionInSpace();
            Quadtree<T> n = getElementsForPosition(c);
            n.elements.add(t);
        }
    }

    public List<T> getElements(Vector3f v) {
        return new ArrayList<>(getElementsForPosition(v).elements);
    }
}
