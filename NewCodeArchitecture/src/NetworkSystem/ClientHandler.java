package NetworkSystem;

import Msg.*;

import java.net.Socket;

public class ClientHandler {

    private Socket socket;

    private Receiver receiver;
    private Sender sender;

    public ClientHandler(Socket socket) {
        this.socket = socket;

        // ObjectInputStream needs to be init before ObjectOutputStream
        // Receiver would block indefinitely
        sender = new Sender(socket);
        receiver = new Receiver(this, socket);

        Thread senderThread = new Thread(sender);
        Thread receiverThread = new Thread(receiver);

        senderThread.start();
        receiverThread.start();
    }


    public void blockingPut(Msg msg) throws InterruptedException {
        sender.blockingPut(msg);
    }

    public boolean isClosed() {
        return socket.isClosed();
    }

    public void close() {
        try {
            blockingPut(new StopMsg());
//            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send(Msg msg) { sender.send(msg); }
}
