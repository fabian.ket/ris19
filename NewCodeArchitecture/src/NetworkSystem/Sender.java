package NetworkSystem;

import Msg.*;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class Sender implements Runnable {

    private Socket socket;
    private ObjectOutputStream oos;

    private BlockingDeque<Msg> msgs = new LinkedBlockingDeque<>();

    public Sender(Socket s) {
        socket = s;

        try {
            oos = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public void blockingPut(Msg msg) throws InterruptedException {
        msgs.put(msg);
    }

    @Override
    public void run() {

        while (!socket.isClosed() && oos != null) {

            try {
                Msg msg = msgs.take();

                if (msg.type() == EMsgType.STOP) {
                    socket.close();
                    break;
                }

                oos.writeUnshared(msg);
                oos.flush();
                oos.reset();
            }
            catch (Exception e) {
                // e.printStackTrace();
                break;
            }
        }
    }

    public void send(Msg msg) {
        try {
            oos.writeUnshared(msg);
            oos.flush();
            oos.reset();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
