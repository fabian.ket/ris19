package NetworkSystem;

import Msg.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class Receiver implements Runnable {

    private ClientHandler clientHandler;
    private Socket socket;
    private ObjectInputStream ois;

    public Receiver(ClientHandler ch, Socket s) {

        clientHandler = ch;
        socket = s;

        try {
            ois = new ObjectInputStream(socket.getInputStream());
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    @Override
    public void run() {

        while (!socket.isClosed() && ois != null) {
            try {
                Msg msg = (Msg) ois.readObject();

                // TODO: hacky should not be done this way
                if (msg.type() == EMsgType.LOGIN) {
                    LoginMsg loginMsg = (LoginMsg) msg;
                    loginMsg.clientHandler = clientHandler;
                }

                NetManager.getInstance().blockingPut(msg);
            }
            catch (Exception e) {
                // e.printStackTrace();
                break;
            }
        }
    }
}
