package NetworkSystem;

import Game.Game;
import GraphicSystem.Renderer;
import Msg.*;
import MsgHandler.Handler;
import MsgHandler.IMsgHandler;
import Server.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


// TODO: Socket connection state needs to be checked
public class NetManager extends Handler<Msg> {

    public int verbose = 0;

    private static class InstanceHolder {
        private static NetManager INSTANCE = new NetManager();
    }

    private ENetManagerType type;
    private String host = "";
    private int port = 12345;

    private ServerSocket server;
    private final List<ClientHandler> clientHandlers = new LinkedList<>();

    // private Thread worker;
    private Thread handlerThread;
    private Thread acceptThread;


    private NetManager() {}


    public static NetManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void init(ENetManagerType type, String host, int port) {
        this.type = type;
        this.host = host;
        this.port = port;

        handlerThread = new Thread(this::handle);
        handlerThread.start();
    }

    private void handle() {

        boolean result = init();

        // TODO: needs to be changed, currently server doesn't uses Game instance
        while (true) {
            try {
                Msg msg = blockingTake();
                checkClientHandlers();
                handle(msg);

                if (msg.type() == EMsgType.STOP) {
                    break;
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }

        exit();
    }

    private boolean init() {
        boolean result;

        if (type == ENetManagerType.SERVER) {
            result = initServer();
        }
        else {
            result = connectToServer();
        }

        if (!result) {
            // TODO: maybe a little bit hard
            System.exit(-1);
        }

        if (type == ENetManagerType.CLIENT)
            Game.getInstance().synchronizeInit();

        return true;
    }

    private boolean initServer() {
        boolean result;

        try {
            server = new ServerSocket(port);
            result = true;

            acceptThread = new Thread(this::accept);
            acceptThread.start();

        } catch (IOException io) {
            io.printStackTrace();
            result = false;
        }

        if (result) {
            register(new IMsgHandler<LoginMsg>() {
                @Override
                public void handle(LoginMsg msg) {
                    try {
                        Server.getInstance().blockingPut(msg);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                @Override
                public EMsgType type() { return EMsgType.LOGIN; }
            });

            register(new IMsgHandler<SendPlayerMsg>() {
                @Override
                public void handle(SendPlayerMsg msg) {
                    try {
                        Server.getInstance().blockingPut(msg);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                @Override
                public EMsgType type() { return EMsgType.SEND_PLAYER; }
            });

            register(new IMsgHandler<PlayerMsg>() {
                @Override
                public void handle(PlayerMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.PLAYER; }
            });

            register(new IMsgHandler<EventBackgroundColorMsg>() {
                @Override
                public void handle(EventBackgroundColorMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.EVENT_BACKGROUND_COLOR; }
            });

            register(new IMsgHandler<EnemyMsg>() {
                @Override
                public void handle(EnemyMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.ENEMY; }
            });

            register(new IMsgHandler<SendChatMsg>() {
                @Override
                public void handle(SendChatMsg msg) { send(new ChatMsg(msg)); }

                @Override
                public EMsgType type() { return EMsgType.SEND_CHAT; }
            });

            register(new IMsgHandler<PlayerShotMsg>() {
                @Override
                public void handle(PlayerShotMsg msg) {
                    try {
                        Server.getInstance().blockingPut(msg);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                @Override
                public EMsgType type() { return EMsgType.PLAYER_SHOT; }
            });

            register(new IMsgHandler<ShotMsg>() {
                @Override
                public void handle(ShotMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.SHOT; }
            });

            register(new IMsgHandler<RemoveShotMsg>() {
                @Override
                public void handle(RemoveShotMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.REMOVE_SHOT; }
            });

            register(new IMsgHandler<EnemyKilledMsg>() {
                @Override
                public void handle(EnemyKilledMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.ENEMY_KILLED; }
            });

            register(new IMsgHandler<SendLogoutMsg>() {
                @Override
                public void handle(SendLogoutMsg msg) {
                    try {
                        Server.getInstance().blockingPut(msg);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                @Override
                public EMsgType type() { return EMsgType.SEND_LOGOUT; }
            });

            register(new IMsgHandler<LogoutMsg>() {
                @Override
                public void handle(LogoutMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.LOGOUT; }
            });
        }

        return result;
    }

    private boolean connectToServer() {
        boolean result = false;

        int tries = 3;

        for (int i = 0; i < tries; i++) {
            try {

                Socket server = new Socket(host, port);

                synchronized (clientHandlers) {
                    clientHandlers.add(new ClientHandler(server));
                }

                result = true;
                break;
            } catch (IOException io) {
                io.printStackTrace();
                result = false;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }

        if (result) {
            // TODO: register IHandlers
            register(new IMsgHandler<LoginMsg>() {
                @Override
                public void handle(LoginMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.LOGIN; }
            });

            register(new IMsgHandler<InitGameMsg>() {
                @Override
                public void handle(InitGameMsg msg) {
                    try {
                        Game.getInstance().blockingPut(msg);
                        if (verbose > 0) {
                            System.out.println("Handled InitGameMsg");
                        }
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                @Override
                public EMsgType type() { return EMsgType.INIT_GAME; }
            });

            register(new IMsgHandler<SendPlayerMsg>() {
                @Override
                public void handle(SendPlayerMsg msg) {
                    send(msg);
                }

                @Override
                public EMsgType type() { return EMsgType.SEND_PLAYER; }
            });

            register(new IMsgHandler<PlayerMsg>() {
                @Override
                public void handle(PlayerMsg msg) {
                    try {
                        Game.getInstance().blockingPut(msg);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                @Override
                public EMsgType type() { return EMsgType.PLAYER; }
            });

            register(new IMsgHandler<EventBackgroundColorMsg>() {
                @Override
                public void handle(EventBackgroundColorMsg msg) {
                    try {
                        // TODO: direct renderer call
                        Renderer.getInstance().blockingPut(msg);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                @Override
                public EMsgType type() { return EMsgType.EVENT_BACKGROUND_COLOR; }
            });

            register(new IMsgHandler<EnemyMsg>() {
                @Override
                public void handle(EnemyMsg msg) {
                    try {
                        Game.getInstance().blockingPut(msg);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                @Override
                public EMsgType type() { return EMsgType.ENEMY; }
            });

            register(new IMsgHandler<SendChatMsg>() {
                @Override
                public void handle(SendChatMsg msg) { send(msg); }

                @Override
                public EMsgType type() { return EMsgType.SEND_CHAT; }
            });

            register(new IMsgHandler<ChatMsg>() {
                @Override
                public void handle(ChatMsg msg) { System.out.println(msg.msg); }

                @Override
                public EMsgType type() { return EMsgType.CHAT; }
            });
        }

        register(new IMsgHandler<PlayerShotMsg>() {
            @Override
            public void handle(PlayerShotMsg msg) { send(msg); }

            @Override
            public EMsgType type() { return EMsgType.PLAYER_SHOT; }
        });

        register(new IMsgHandler<ShotMsg>() {
            @Override
            public void handle(ShotMsg msg) {
                try {
                    Game.getInstance().blockingPut(msg);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public EMsgType type() { return EMsgType.SHOT; }
        });

        register(new IMsgHandler<RemoveShotMsg>() {
            @Override
            public void handle(RemoveShotMsg msg) {
                try {
                    Game.getInstance().blockingPut(msg);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public EMsgType type() { return EMsgType.REMOVE_SHOT; }
        });

        register(new IMsgHandler<EnemyKilledMsg>() {
            @Override
            public void handle(EnemyKilledMsg msg) {
                try {
                    Game.getInstance().blockingPut(msg);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public EMsgType type() { return EMsgType.ENEMY_KILLED; }
        });

        register(new IMsgHandler<SendLogoutMsg>() {
            @Override
            public void handle(SendLogoutMsg msg) {
                send(msg);
            }

            @Override
            public EMsgType type() { return EMsgType.SEND_LOGOUT; }
        });

        register(new IMsgHandler<LogoutMsg>() {
            @Override
            public void handle(LogoutMsg msg) {
                try {
                    Game.getInstance().blockingPut(msg);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public EMsgType type() { return EMsgType.LOGOUT; }
        });

        return result;
    }

    private void accept() {

        // TODO: needs to be changed, currently server doesn't uses Game instance
        while (Game.getInstance().getRun()) {
            try {
                Socket client = server.accept();

                synchronized (clientHandlers) {
                    clientHandlers.add(new ClientHandler(client));
                }

                if (verbose > 0) {
                    System.out.println("Client connected!");
                }

            } catch (IOException io) {
                // io.printStackTrace();
            }
        }
    }

    private void send(Msg msg) {
        synchronized (clientHandlers) {
            for (ClientHandler ch : clientHandlers) {
                try {
                    ch.blockingPut(msg);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }

    private void checkClientHandlers() {
        List<ClientHandler> closedOnes = new ArrayList<>();

        synchronized (clientHandlers) {
            for (ClientHandler ch : clientHandlers) {
                if (ch.isClosed()) {
                    closedOnes.add(ch);
                }
            }

            for (ClientHandler ch : closedOnes) {
                ch.close();
                clientHandlers.remove(ch);
            }
        }
    }

    public void exit() {
        for (ClientHandler ch : clientHandlers) {
             ch.close();
        }

        if (server != null) {

            try {
                server.close();
            } catch (IOException io) {
                io.printStackTrace();
            }
        }
    }

    public void sendDirectly(Msg msg) {
        synchronized (clientHandlers) {
            for (ClientHandler ch : clientHandlers) {
                ch.send(msg);
            }
        }
    }
}
