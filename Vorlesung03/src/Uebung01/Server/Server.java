package Uebung01.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;

public class Server {

    public static void main(String[] args) {

        int port = 12345;

        Queue<Socket> clients = new LinkedList<>();

        Object clientsLock = new Object();

        try (ServerSocket serverSocket = new ServerSocket(port)) {

            Thread acceptThread = new Thread(() -> {

                while(true) {
                    Socket c = null;

                    try {
                        c = serverSocket.accept();
                    } catch (IOException io) {
                        io.printStackTrace();
                    }

                    synchronized (clientsLock) {
                        if (c != null)
                            clients.add(c);
                    }
                }
            });

            acceptThread.start();

            boolean next = true;
            int data = 0;

            Queue<String> msgs = new LinkedList<>();

            // while (next || clients.size() == 0) {
            while (true) {
                System.out.println(String.format("Clients connected: %d", clients.size()));

                synchronized (clientsLock) {
                    next = false;

                    for (Socket client : clients) {
                        if (client.isConnected()) {
                            BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));

                            if (br.ready()) {
                                msgs.add(br.readLine());
                            }
                            next = true;
                        }
                    }

                    for (Socket client : clients) {
                        for (String msg : msgs) {
                            PrintWriter pw = new PrintWriter(client.getOutputStream(), true);
                            pw.println(msg);
                        }
                    }
                }

                msgs.clear();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }

//            synchronized (clientsLock) {
//                for (Socket client : clients) {
//                    client.close();
//                }
//            }

        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
