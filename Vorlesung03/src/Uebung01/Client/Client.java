package Uebung01.Client;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        int port = 12345;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        Console cmd = System.console();

        try (Socket server = new Socket("", port)) {

            BufferedReader serverIn = new BufferedReader(new InputStreamReader(server.getInputStream()));
            PrintWriter serverOut = new PrintWriter(server.getOutputStream(), true);

            Thread cmdPrinter = new Thread(() -> {
                String msg;

                try {
                    while (true) {
                        if (serverIn.ready()) {
                            msg = serverIn.readLine();
                            System.out.println(String.format("Client received: %s", msg));
                        }
                    }
                } catch (IOException io) {
                    io.printStackTrace();
                }
            });

            cmdPrinter.start();

            while (true) {

                // String msg = br.readLine();

                String in = cmd.readLine();
                serverOut.println(in);
            }

        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
